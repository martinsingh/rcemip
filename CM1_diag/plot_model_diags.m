%
% Zero-buoyancy plume diagnosis of RCEMIP simulations. Some tests.
%

%% Options %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%model = 3; % plot a set of profiles for a single model (CM1 is example)
%SSTs = 300;

model =  'all'; % plot relationshps among all models
SSTs = [295 300 305];



%entrainment_type = 'invz';
entrainment_type = 'const';

% Defintion of lower troposphere
z_lower = 1000;
z_mid = 5000;

    
% Parcel parameters (for CAPE calculations)
gamma = 1;          % pseudoadiabatic
deltaT = 40;        % mixed-phase range
ice = 1;            % use ice
use_Tv = 1;         % use virtual temperature correction



model_type = 'small';



%% Initialize  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load the thermodynamics
c = atm.load_constants;

% Set the models we are doing
if strcmp(model,'all')
   models = [1:5 7:20 23:28]; % These are the models that I could get to work
else
    models = model; 
end


epsilon_diag = nan(length(models),length(SSTs));
PE_diag = nan(length(models),length(SSTs));

MCAPE_LT = nan(length(models),length(SSTs));
MCAPE_LT_ZBP = nan(length(models),length(SSTs));

CAPE_LT = nan(length(models),length(SSTs));
CAPE_LT_ZBP = nan(length(models),length(SSTs));

MCAPE_FT = nan(length(models),length(SSTs));
MCAPE_FT_ZBP = nan(length(models),length(SSTs));

CAPE_FT = nan(length(models),length(SSTs));
CAPE_FT_ZBP = nan(length(models),length(SSTs));
CAPE_ZBP_Romps = nan(length(models),length(SSTs));

CAPE = nan(length(models),length(SSTs));





% Loop over all models
for i = 1:length(models)
for k = 1:length(SSTs)

    %% Get the data for the model %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    SST = SSTs(k);

    % Read data
    [z,T,p,Tv,qv,RH,rho,model_name] = load_model(models(i),SST,model_type);
    Nmod = length(z);
    disp([model_name ' : ' num2str(SST)])
    


    % Calculate lower tropospheric levels
    [il,im,zl,zm,Tl,pl] = calculate_lower_trop(z_lower,z_mid,z,T,p);
    Ilm = il:im;    % Lower tropospheric levels
    Ilt = il:Nmod;  % levels above z_lower


    %% Diagnose entrainment and precipitation efficiency %%%%%%%%%%%%%%%%

    % Diagnosis based on zero-buoyancy plume profiles with varying epsilon and PE
    [epsilon_diag(i,k),PE_diag(i,k)] = diagnose_entrainment(zl,zm,models(i),SST,model_type,entrainment_type);

    % Initialize ZBP profiles
    T_ZBP  = nan([Nmod 1]);
    Tm_ZBP = nan([Nmod 1]);
    RH_ZBP = nan([Nmod 1]);

    % Calculate the ZBP profiles based on the diagnosed parameters
    [T_ZBP(Ilt),~,~,RH_ZBP(Ilt),Tm_ZBP(Ilt)] = ...
        calculate_ZBP(z(Ilt),p(Ilt),Tl,epsilon_diag(i,k),PE_diag(i,k),entrainment_type);


    % This just fixes up some weird results at very high heights
    % I need to work out exactly why this happens
    T_ZBP = real(T_ZBP);
    Tm_ZBP = real(Tm_ZBP);


    %% Compare Lower tropospheric CAPE from ZBP and model %%%%%%%%%%%%%%%

    % Calculate the MCAPE in the lower troposphere for the model profile
    MCAPE_LT(i,k) = c.g.*trapz(z(Ilm), (Tm_ZBP(Ilm) - T(Ilm))./T(Ilm) );

    % Calculate the MCAPE in the lower troposphere implied by the ZBP profile
    MCAPE_LT_ZBP(i,k) = c.g.*trapz(z(Ilm), (Tm_ZBP(Ilm) - T_ZBP(Ilm))./T_ZBP(Ilm) );

    % Calculate CAPE for lower troposphere

    % initialise parcel at saturation at 1 km
    T1km = Tl;
    p1km = pl;
    rs1km = atm.r_sat(T1km,p1km);

    % Lower tropospheric CAPE (including Tv)
    [CAPE_LT(i,k),~,~] = calculate_CAPE(T1km,rs1km,p1km,Tv,p,z,gamma,deltaT,ice,use_Tv,zm);

    % Lower tropospheric CAPE of the ZBP model
    [CAPE_LT_ZBP(i,k),~,~] = calculate_CAPE(T1km,rs1km,p1km,T_ZBP,p,z,gamma,deltaT,ice,use_Tv,zm);

   

    %% Compare full-tropospheric CAPE for model and ZBP profile %%%%%%%%%%%%%%%%%%%%%%%

    % Calculate full tropospheric CAPE (initialised from saturated parcel
    % at z_lower

    % Calculate the MCAPE for the model profile
    ilnb = find(Tm_ZBP>T,1,'last');
    Icape = il:ilnb;
    MCAPE_FT(i,k) = c.g.*trapz(z(Icape), (Tm_ZBP(Icape) - T(Icape))./T(Icape) );

    % Calculate the MCAPE in of the ZBP profile
    MCAPE_FT_ZBP(i,k) = c.g.*trapz(z(Icape), (Tm_ZBP(Icape) - T_ZBP(Icape))./T_ZBP(Icape) );




    % calculate moist adiabat initialised at 1 km
    [T_par,r_par,rl_par,ri_par,T_rho_par] = atm.calculate_adiabat(T1km,rs1km,p1km,p,gamma,'default',deltaT,ice);
 
    % CAPE (including Tv)
    [CAPE_FT(i,k),p_LNB,T_rho_LNB] = calculate_CAPE(T1km,rs1km,p1km,Tv,p,z,gamma,deltaT,ice,use_Tv,0);
    [~,ILNB] = min(abs(p-p_LNB));
    zLNB = z(ILNB);

    % CAPE for ZBP profile (start from 1 km, up to the same level as in the model sounding)
    [CAPE_FT_ZBP(i,k),~,~] = calculate_CAPE(T1km,rs1km,p1km,T_ZBP,p,z,gamma,deltaT,ice,use_Tv,zLNB);

    % Calculate the full-tropospheric CAPE based on the Romps formula
    [CAPE_ZBP_Romps(i,k),~,~] = calculate_CAPE_theory(Tl,T_rho_LNB,pl,epsilon_diag(i),PE_diag(i));



    %[T_par2,r_par,rl_par,ri_par,T_rho_par] = atm.calculate_adiabat(T1km,rs1km,p1km,p,gamma,'default',deltaT,ice);
 
    %% Now compare actual CAPE that is initialised with a surface parcel


    % Parcel properties
    Tsurf = T(1);
    psurf = p(1);
    rsurf = qv(1)./(1-qv(1));

     % calculate moist adiabat initialised at 1 km
    [T_par,r_par,rl_par,ri_par,T_rho_par] = atm.calculate_adiabat(Tsurf,rsurf,psurf,p,gamma,'default',deltaT,ice);
 
    % CAPE (including Tv)
    [CAPE(i,k),p_LNB,T_rho_LNB] = calculate_CAPE(Tsurf,rsurf,psurf,Tv,p,z,gamma,deltaT,ice,use_Tv,0);

 
    %clf
    %plot(T_par-T,z./1000)
    %hold on
    %plot(T_par2(Il:end)-T_ZBP,z(Il:end)./1000)


    %plot(Tm_ZBP-T(Il:end),z(Il:end)./1000)
    %hold on
    %plot(Tm_ZBP-T_ZBP,z(Il:end)./1000)
    %set(gca,'xlim',[-10 10],'ylim',[0 17])
    %pause

    
end
end

%% Plot the CAPE and ZBP CAPE against eachother
fig.bfig(20,30)

L = [0.1 0.55];
B = [0.07 0.4 0.73];
W = 0.35;
H = 0.24;

axes('position',[L(1) B(1) W H])
plot(MCAPE_LT_ZBP,MCAPE_LT,'o')
xlabel('ZBP lower-troposperic MCAPE (J/kg)')
ylabel('lower-troposperic MCAPE (J/kg)')
R = corrcoef(MCAPE_LT_ZBP(:),MCAPE_LT(:));
title(['R^2 = ' num2str(R(2).^2)])
set(gca,'xlim',[0 max(MCAPE_LT_ZBP(:)).*1.05])
set(gca,'ylim',[0 max(MCAPE_LT_ZBP(:)).*1.05])
hold on
plot([0 1e6],[0 1e6],'k--')

axes('position',[L(2) B(1) W H])
plot(CAPE_LT_ZBP,CAPE_LT,'o')
xlabel('ZBP lower-troposperic CAPE (J/kg)')
ylabel('lower-troposperic CAPE (J/kg)')
R = corrcoef(CAPE_LT_ZBP(:),CAPE_LT(:));
title(['R^2 = ' num2str(R(2).^2)])
set(gca,'xlim',[0 max(CAPE_LT_ZBP(:)).*1.05])
set(gca,'ylim',[0 max(CAPE_LT_ZBP(:)).*1.05])
hold on
plot([0 1e6],[0 1e6],'k--')

axes('position',[L(1) B(2) W H])
plot(MCAPE_FT_ZBP,MCAPE_FT,'o')
xlabel('ZBP full-troposperic MCAPE (J/kg)')
ylabel('full-troposperic MCAPE (J/kg)')
R = corrcoef(MCAPE_FT_ZBP(:),MCAPE_FT(:));
title(['R^2 = ' num2str(R(2).^2)])
set(gca,'xlim',[0 max(MCAPE_FT_ZBP(:)).*1.05])
set(gca,'ylim',[0 max(MCAPE_FT_ZBP(:)).*1.05])
hold on
plot([0 1e6],[0 1e6],'k--')

axes('position',[L(2) B(2) W H])
plot(CAPE_FT_ZBP,CAPE_FT,'o')
xlabel('ZBP full-troposperic CAPE (J/kg)')
ylabel('full-troposperic CAPE (J/kg)')
R = corrcoef(CAPE_FT_ZBP(:),CAPE_FT(:));
title(['R^2 = ' num2str(R(2).^2)])
set(gca,'xlim',[0 max(CAPE_FT_ZBP(:)).*1.05])
set(gca,'ylim',[0 max(CAPE_FT_ZBP(:)).*1.05])
hold on
plot([0 1e6],[0 1e6],'k--')


axes('position',[L(1) B(3) W H])
plot(CAPE_FT_ZBP,CAPE_ZBP_Romps,'o')
xlabel('ZBP full-troposperic CAPE (J/kg)')
ylabel('Romps full-troposperic CAPE (J/kg)')
R = corrcoef(CAPE_FT_ZBP(:),CAPE_ZBP_Romps(:));
title(['R^2 = ' num2str(R(2).^2)])
set(gca,'xlim',[0 max(CAPE_FT_ZBP(:)).*1.05])
set(gca,'ylim',[0 max(CAPE_FT_ZBP(:)).*1.05])
hold on
plot([0 1e6],[0 1e6],'k--')

axes('position',[L(2) B(3) W H])
plot(CAPE_FT,CAPE,'o')
xlabel('full-troposperic CAPE (J/kg)')
ylabel('CAPE (J/kg)')
R = corrcoef(CAPE_FT(:),CAPE(:));
title(['R^2 = ' num2str(R(2).^2)])
set(gca,'xlim',[0 max(CAPE(:)).*1.05])
set(gca,'ylim',[0 max(CAPE(:)).*1.05])
hold on
plot([0 1e6],[0 1e6],'k--')


print -dpdf ../Figures/CAPE_ZBP_comparisons.pdf


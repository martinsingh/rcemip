function [epsilon_diag,PE_diag] = diagnose_entrainment(zl,zm,varargin)
%
% Diagnose the entrainment and precip efficiency from a simulation
% based on the zero-buoyancy plume model
%
% The diagnosis is based on two diagnostics
%
% 1) lower-tropospheric instability index: 
%    Integral of positive buoyancy of plume with zero entrainment in the
%    lower troposphere
%
% 2) lower-tropospheric relative humidity
%    Mass-weighted mean lower-tropospheric humidity
%
% Lower troposphere is defined by input parameters
%
% Inptus:
%
%     zl and zm = the lower and upper limits of the lower troposphere (m)
%
% Optional:
%
%     model_name = name of model (or number)
%     SST = simulation sea-surface temperature (295, 300, 305)
%     sim_type = 'large' | 'small'
%
%
% Outputs:
%
%     epsilon_diag = diagnosed entrainment rate of the simulation
%     PE_diag = diagnosed precipitation efficiency of simulation

%% Input parsing %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Defaults
model_name = 'CM1-LES';
SST = 300;
sim_type = 'small';
entrainment_type = 'const';

% Parse inputs
if nargin >= 3;  model_name = varargin{1}; end
if nargin >= 4;  SST = varargin{2}; end
if nargin >= 5;  sim_type = varargin{3}; end
if nargin >= 6;  entrainment_type = varargin{4}; end


%% Load data from simulation and calculate the diagnostics %%%%%%%%%%%%%%
c = atm.load_constants;

[z,T,p,Tv,qv,RH,rho,model_name] = load_model(model_name,SST,sim_type);



% Calculate the range of heights that we require
[~,Im] = min(abs(z-zm));
zm = z(Im);

[~,Il] = min(abs(z-zl));
zl = z(Il);

I = false(length(z),1); I(Il:Im) = true;
  
% Calculate quantities at bottom level
Tl = T(Il);
pl = p(Il);

% Calculate zero-entrainment temperature profile
[Tm,~,~,~,~] = calculate_ZBP(z(I),p(I),Tl,0,1);

% Integral of buoyancy over lower troposphere
thermo_var = c.g.*trapz(z(I),(Tm-T(I))./T(I));          

% Calculate lower-tropospheric humidity
hum_var = trapz(z(I),RH(I).*rho(I))./trapz(z(I),rho(I));



%% Calculate the ZBP diagnostics %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       
c = atm.load_constants;

% Calculate the temperarture profile for a range of precipitation
% efficiencies and entrainment rates
PE      = 0:0.02:1;
epsilon = [0:0.01:0.1 0.2:0.02:2 2.1:0.13 3.5 4 4.5 5 5.5 6 6.5 7 7.5 8].*1e-3;

% make some matrices
PE_mat      = repmat(PE',[1,length(epsilon)]);
epsilon_mat = repmat(epsilon,[length(PE),1]);
Tl_mat      = Tl.*ones(size(epsilon_mat));
pl_mat      = pl.*ones(size(epsilon_mat));


% ZBP solution for profiles
[T,p,z,RH,Tm] = calculate_ZBP([zl zm],pl_mat,Tl_mat,epsilon_mat,PE_mat,entrainment_type);

% Calculate the diagnostics from the ZBP model
thermo_var_ZBP = squeeze(c.g.*trapz(z,(Tm-T)./T));
hum_var_ZBP = squeeze( trapz(z,RH.*p./(c.Rd.*T)) ./ trapz(z,p./(c.Rd.*T)) );


%% Diagnose the parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Diagnose the entrainment and precip. efficiency from the simulation

% Method:
% Calculate contours of CAPE and RH for the simulated values and
% see where they intersect.
% Also tried MATLAB's "scattered interpolant" function, but this
% produced inaccurate results for the entrainment. Not sre why

% The contours
C1 = contourc(epsilon,PE,thermo_var_ZBP,[thermo_var thermo_var]);
C2 = contourc(epsilon,PE,hum_var_ZBP,[hum_var hum_var]);

% Find intersection
[epsilon_diag,PE_diag] = polyxpoly(C1(1,2:end),C1(2,2:end),C2(1,2:end),C2(2,2:end));






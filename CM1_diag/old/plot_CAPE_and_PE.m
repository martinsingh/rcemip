
%% Calculate CAPE as a function of RH from theory %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Tb = 300;                   % Cloud base temperature (K)
pb = 95000;                 % Cloud base pressure (pa)

Tt = 200;                   % Temperature of top of convecting layer (K)
                            % FAT assumption

           
% Definition of lower troposphere                            
zl = 1000;
zm = 5000;
                            
% Plot the solution for a range of precipitation efficiencies and entrainment rates
PE_R16 = [0 0.01 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1];
epsilon_R16 = [0 0.01 0.02 0.05 0.1:0.1:2 2,5 3].*1e-3;


% Initialize the matrices
RH_R16 = nan(length(PE_R16),length(epsilon_R16));
CAPE_R16 = nan(length(PE_R16),length(epsilon_R16));
CAPE_R16_simple = nan(length(PE_R16),length(epsilon_R16));

% Calculate this in a loop because I am lazy
for i = 1:length(PE_R16)
    for j = 1:length(epsilon_R16)

       [CAPE_R16(i,j),RH_R16(i,j),CAPE_R16_simple(i,j)] = calculate_CAPE_theory(Tb,Tt,pb,epsilon_R16(j),PE_R16(i));

    end
end

PE_R16_mat = repmat(PE_R16',[1,length(epsilon_R16)]);

%% Calculate the CAPE and RH from theory and compare to the theoretical values


% Experiments to plot
expts = {...
'RCEMIPN96_dx1000.0_SST300'...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow10.0_arain10.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow10.0_arain10.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow1.0_arain1.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow1.0_arain1.0_thic0.1_thcl0.1' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow1.0_arain1.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow5.0_arain5.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow5.0_arain5.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt-1_asnow10.0_arain10.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt-1_asnow1.0_arain1.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt-1_asnow3.0_arain3.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt-1_asnow5.0_arain5.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt2.0_asnow1.0_arain1.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt8.0_asnow1.0_arain1.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt8.0_asnow5.0_arain5.0_thic1.0_thcl1.0' ...
};



for i =1:length(expts)

   % Load mean data (saved with save_mean.m)
   load(['./mat_data/' expts{i} '_mean.mat'])

   % Calculate preciptiation efficiency
   PE(i) = P_mean./P_gross;

   % Calculate proxy preciptiation efficiency
   PE_proxy(i) = P_mean./(qc_path_mean+qi_path_mean+qs_path_mean+qr_path_mean+qg_path_mean);

   % Calculate lower-tropospheric humidity
   lower_trop = z>zl & z<=zm;
   RH_trop(i) = sum(RH_mean.*rho_mean.*lower_trop.*dz)./sum(rho_mean.*lower_trop .*dz);

   [~,Im] = min(abs(z-zm));
   [~,Il] = min(abs(z-zl));
   lr_trop(i) =- ( Trho_mean(Im) - Trho_mean(Il) ) ./ ( z(Im) - z(Il));
   
   % Calculate CAPE
   Tinit = T_mean(1);
   rvinit = rv_mean(1);
   pinit = p_mean(1);
   CAPE(i) = calculate_CAPE(Tinit,rvinit,pinit,Trho_mean,p_mean);

   % Calculate the theoretical PE and entrainment based on the CAPE and lower-tropospheric RH
   PE_theory(i) = griddata(double(RH_R16(2:end,:)),double(CAPE_R16(2:end,:)),double(PE_R16_mat(2:end,:)),double(RH_trop(i)),double(CAPE(i)));


end





%% Plot figure showing CAPE vs RH for R16 solution

% Which values should we plot
PE_plot = [0.2 0.4 0.6 0.8 1];
eps_plot = [0.05 0.1 0.2 0.4 0.8].*1e-3;

% Which values do we label
PE_lab = [0.2 0.6  1];
PE_pos = [2500 3500 2000];
eps_lab = [0.05 0.1 0.2 0.4 0.8].*1e-3;
eps_pos = [0.7 0.7 0.7 0.7 0.9];

% Make the figure
fig.bfig(16,12)

set(gca,'ylim',[0 6000])
set(gca,'xlim',[0 1])
hold on

% Plot the lines of constant precipitation efficiency
for i = 1:length(PE_plot)

    % Find the precipitation efficiency we want
    I = find(PE_R16==PE_plot(i));

    % Plot the line
    pp = plot(RH_R16(I,:),CAPE_R16(I,:),'k');

    % Label the line
    if ismember(PE_plot(i),PE_lab)
        j = find(PE_lab==PE_plot(i));
        ll = fig.inline_label(pp,['PE = ' num2str(PE_plot(i))],[],PE_pos(j));
    end

end


% Plot the lines of constant entrainment rate
for i = 1:length(eps_plot)

    % Find the precipitation efficiency we want
    I = find(epsilon_R16==eps_plot(i));

    % Plot the line
    pp = plot(RH_R16(:,I),CAPE_R16(:,I),'color',[0.5 0.5 0.5]);

    % Label the line
    if ismember(eps_plot(i),eps_lab)
       j = find(eps_lab==eps_plot(i));
       ll = fig.inline_label(pp,['\epsilon = ' num2str(eps_plot(i).*1000)],eps_pos(j),[],'color',[0.5 0.5 0.5]);
    end

end


scatter(RH_trop,CAPE,20,PE,'filled')

cc = colorbar
ylabel(cc,'precip. efficiency')

set(gca,'xlim',[0.5 1],'ylim',[0 5000])

xlabel('relative humidity')
ylabel('CAPE(J kg^{-1})')
box off


print -dpdf ../Figures/Phase_diagram_altered_mic.pdf


%% Plot PE versus proxy and theory

fig.bfig(8,17)
subplot(311)
plot(PE,PE_theory,'o')
hold on
plot([0 1],[0 1],'-','color',[0.5 0.5 0.5])

pp = polyfit(PE,PE_theory,1);
Rsquared = corr(PE',PE_theory').^2;
x = 0:0.1:1;
plot(x,pp(1).*x+pp(2),'k--')
text(0.05,0.4,['R^2 = ' num2str(Rsquared)])
%xlabel('precipitation efficiency')
ylabel('theoretical precip. efficiency')
set(gca,'xlim',[0 0.6],'ylim',[0 0.5])

box off

subplot(312)

plot(PE,PE_proxy,'o')
hold on

set(gca,'xlim',[0 0.6],'ylim',[0 1.7e-3])

pp = polyfit(PE,PE_proxy,1);
Rsquared = corr(PE',PE_proxy').^2;
x = 0:0.1:1;
plot(x,pp(1).*x+pp(2),'k--')
text(0.05,1.3e-3,['R^2 = ' num2str(Rsquared)])

set(gca,'xlim',[0 0.6],'ylim',[0 1.7e-3])

%xlabel('precipitation efficiency')
ylabel('proxy precip. efficiency')

box off


subplot(313)

plot(PE,RH_trop,'o')
hold on



pp = polyfit(PE,RH_trop,1);
Rsquared = corr(PE',RH_trop').^2;
x = 0:0.1:1;
plot(x,pp(1).*x+pp(2),'k--')
text(0.05,0.65,['R^2 = ' num2str(Rsquared)])

set(gca,'xlim',[0 0.6],'ylim',[0.6 1])

xlabel('precipitation efficiency')
ylabel('tropospheric RH')


box off

print -dpdf ../Figures/precip_efficiency.pdf

%% Plot PE versus



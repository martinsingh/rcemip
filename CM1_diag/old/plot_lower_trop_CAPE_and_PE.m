
%% Calculate CAPE as a function of RH from theory %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% For 1 km
Tl_ZBP = 285;                   % Cloud base temperature (K)
pl_ZBP = 89400;                 % Cloud base pressure (pa)

% For 2 km
%Tl_ZBP = 279;                   % Cloud base temperature (K)
%pl_ZBP = 79300;                 % Cloud base pressure (pa)


           
% Definition of lower troposphere                            
zl = 1000;
zm = 5000;
                            
% Plot the solution for a range of precipitation efficiencies and entrainment rates
PE_ZBP = [0:0.01:1];
epsilon_ZBP = [0 0.01 0.02 0.03 0.04 0.05 0.06:0.02:2 2.1 2.2 2.3 2.4 2.5 2.75 3 3.5 4 5 6 7 8].*1e-3;



% Initialize the matrices
RH_ZBP = nan(length(PE_ZBP),length(epsilon_ZBP));
CAPE_ZBP = nan(length(PE_ZBP),length(epsilon_ZBP));


% Calculate this in a loop because I am lazy
for i = 1:length(PE_ZBP)
    disp(i)
    for j = 1:length(epsilon_ZBP)

       [CAPE_ZBP(i,j),RH_ZBP(i,j)] = calculate_lower_trop_CAPE_theory(Tl_ZBP,zl,pl_ZBP,zm,epsilon_ZBP(j),PE_ZBP(i));



    end
end





% make a matrix
PE_ZBP_mat = repmat(PE_ZBP',[1,length(epsilon_ZBP)]);
epsilon_ZBP_mat = repmat(epsilon_ZBP,[length(PE_ZBP),1]);

%% Calculate the CAPE and RH from sims and compare to the theoretical values


% Experiments to plot
expts = {...
'RCEMIPN96_dx1000.0_SST300'...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow10.0_arain10.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow10.0_arain10.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow1.0_arain1.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow1.0_arain1.0_thic0.1_thcl0.1' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow1.0_arain1.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow5.0_arain5.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow5.0_arain5.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt-1_asnow10.0_arain10.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt-1_asnow1.0_arain1.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt-1_asnow3.0_arain3.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt-1_asnow5.0_arain5.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt2.0_asnow1.0_arain1.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt8.0_asnow1.0_arain1.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt8.0_asnow5.0_arain5.0_thic1.0_thcl1.0' ...
};


RH_trop = zeros(length(expts),1);
CAPE_trop = zeros(length(expts),1);
epsilon_theory = zeros(length(expts),1);
PE_theory = zeros(length(expts),1);

RHc = RH_ZBP(2:end,:);
CAPEc = CAPE_ZBP(2:end,:);
epsc = epsilon_ZBP_mat(2:end,:);
PEc = PE_ZBP_mat(2:end,:);
Feps = scatteredInterpolant(RHc(:),CAPEc(:),epsc(:));
Fleps = scatteredInterpolant(RHc(:),CAPEc(:),log(epsc(:)));
FPE = scatteredInterpolant(RHc(:),CAPEc(:),PEc(:));

for i =1:length(expts)

   % Load mean data (saved with save_mean.m)
   load(['./mat_data/' expts{i} '_mean.mat'])

   % Calculate preciptiation efficiency
   PE(i) = P_mean./P_gross;

   % Calculate proxy preciptiation efficiency
   PE_proxy(i) = P_mean./(qc_path_mean+qi_path_mean+qs_path_mean+qr_path_mean+qg_path_mean);

   % Calculate lower-tropospheric humidity
   lower_trop = z>zl & z<=zm;
   RH_trop(i) = sum(RH_mean.*rho_mean.*lower_trop.*dz)./sum(rho_mean.*lower_trop .*dz);

   % Calculate lower-tropospheric CAPE
   [~,Im] = min(abs(z-zm));
   [~,Il] = min(abs(z-zl));
   CAPE_trop(i) = calculate_lower_trop_CAPE(T_mean(Il:Im),p_mean(Il:Im),z(Il:Im));
   
   
   Tl(i) = T_mean(Il);
   pl(i) = p_mean(Il);
  

   %% Calculate the theoretical PE and entrainment based on the CAPE and lower-tropospheric RH
   
   % First use MATLAB's "scattered interpolant" function for interpolating
   % non-gridded data
   PE_theory2(i) = FPE(RH_trop(i),CAPE_trop(i));
   epsilon_theory2(i) = Feps(RH_trop(i),CAPE_trop(i));
   
   % Now calculate contours of CAPE and RH for the simulated values and
   % seee where they intersect
   
   % The contours
   C1 = contourc(epsilon_ZBP,PE_ZBP,CAPE_ZBP,[CAPE_trop(i) CAPE_trop(i)]);
   C2 = contourc(epsilon_ZBP,PE_ZBP,RH_ZBP,[RH_trop(i) RH_trop(i)]);
   
   % Find intersection
   [epsilon_theory(i),PE_theory(i)] = polyxpoly(C1(1,2:end),C1(2,2:end),C2(1,2:end),C2(2,2:end));
   
   
   
   
   
   
end

%% Some tests
%RH_grid = [0.6:0.05:0.9];
%CAPE_grid = [100:50:600];
%[RH_grid,CAPE_grid] = meshgrid(RH_grid,CAPE_grid);
%PE_grid = FPE(RH_grid,CAPE_grid);
%eps_grid = Feps(RH_grid,CAPE_grid);
%eps_grid2 = exp(Fleps(RH_grid,CAPE_grid));

%subplot(121)
%contourf(RH_grid,CAPE_grid,eps_grid2,[0:0.1:2].*1e-3); colorbar

%subplot(122)
%contourf(RH_ZBP,CAPE_ZBP,epsilon_ZBP_mat,[0:0.1:2].*1e-3); colorbar
%set(gca,'xlim',[0.6 0.9],'ylim',[100 600])


%% Plot figure showing CAPE vs RH for R16 solution

% Which values should we plot
PE_plot = [0.2 0.4 0.6 0.8 1];
eps_plot = [0.0 0.1 0.2 0.4 0.8 1.6].*1e-3;

% Which values do we label
PE_lab = [0.2 0.6  1];
PE_pos = [400 500 600];
eps_lab = [0.0 0.1 0.2 0.4 0.8 1.6].*1e-3;
eps_pos = [0.7 0.7 0.7 0.7 0.9 0.95];

% Make the figure
fig.bfig(16,12)


set(gca,'xlim',[0.5 1],'ylim',[0 700])
hold on

% Plot the lines of constant precipitation efficiency
for i = 1:length(PE_plot)

    % Find the precipitation efficiency we want
    I = find(PE_ZBP==PE_plot(i));

    % Plot the line
    pp = plot(RH_ZBP(I,:),CAPE_ZBP(I,:),'k');

    % Label the line
    if ismember(PE_plot(i),PE_lab)
        j = find(PE_lab==PE_plot(i));
        ll = fig.inline_label(pp,['PE = ' num2str(PE_plot(i))],[],PE_pos(j));
    end

end


% Plot the lines of constant entrainment rate
for i = 1:length(eps_plot)

    % Find the precipitation efficiency we want
    I = find(epsilon_ZBP==eps_plot(i));

    % Plot the line
    pp = plot(RH_ZBP(:,I),CAPE_ZBP(:,I),'color',[0.5 0.5 0.5]);

    % Label the line
    if ismember(eps_plot(i),eps_lab)
       j = find(eps_lab==eps_plot(i));
       ll = fig.inline_label(pp,['\epsilon = ' num2str(eps_plot(i).*1000)],eps_pos(j),[],'color',[0.5 0.5 0.5]);
    end

end


scatter(RH_trop,CAPE_trop,20,PE,'filled')

cc = colorbar;
ylabel(cc,'precip. efficiency')

set(gca,'xlim',[0.5 1],'ylim',[0 700])

xlabel('relative humidity')
ylabel('lower-tropospheric CAPE (J kg^{-1})')
box off


print -dpdf ../Figures/lower_trop_CAPE_altered_mic.pdf


%% Plot PE versus proxy and theory

fig.bfig(8,17)
subplot(311)
plot(PE,PE_theory,'o')
hold on
plot([0 1],[0 1],'-','color',[0.5 0.5 0.5])

pp = polyfit(PE,PE_theory,1);
Rsquared = corr(PE',PE_theory).^2;
x = 0:0.1:1;
plot(x,pp(1).*x+pp(2),'k--')
text(0.05,0.7,['R^2 = ' num2str(Rsquared)])
%xlabel('precipitation efficiency')
ylabel('theoretical precip. efficiency')
set(gca,'xlim',[0 0.6],'ylim',[0 0.75])

box off

subplot(312)

plot(PE,PE_proxy,'o')
hold on

set(gca,'xlim',[0 0.6],'ylim',[0 1.7e-3])

pp = polyfit(PE,PE_proxy,1);
Rsquared = corr(PE',PE_proxy').^2;
x = 0:0.1:1;
plot(x,pp(1).*x+pp(2),'k--')
text(0.05,1.3e-3,['R^2 = ' num2str(Rsquared)])

set(gca,'xlim',[0 0.6],'ylim',[0 1.7e-3])

%xlabel('precipitation efficiency')
ylabel('proxy precip. efficiency')

box off


subplot(313)

plot(PE,RH_trop,'o')
hold on



pp = polyfit(PE,RH_trop,1);
Rsquared = corr(PE',RH_trop).^2;
x = 0:0.1:1;
plot(x,pp(1).*x+pp(2),'k--')
text(0.05,0.65,['R^2 = ' num2str(Rsquared)])

set(gca,'xlim',[0 0.6],'ylim',[0.6 1])

xlabel('precipitation efficiency')
ylabel('tropospheric RH')


box off

print -dpdf ../Figures/precip_efficiency.pdf



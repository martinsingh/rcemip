function [T,p,z,RH] = calculate_ZBP(Tl,z,p,epsilon,PE)
%
% Calculate the temperature and relative humidity profiles
% between two levels zl and zm in the lower troposphere
% based on the zero-buoyancy plume model. 
%
% Inputs:
%
%    Tl = temperature at lower level
%    z  = height of lower and upper levels or height vector
%    p  = pressure of lower level or pressure vector
%
%    epsilon = entrainment rate
%    PE = precipitation efficiency
%

%% Thermodynamics %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c = atm.load_constants;


%% Initialise vectors %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initialise height vector
if length(z) <= 2

   if length(z) ~=2; error('z should be a two element vector giving the lower and upper bound'); end
   zl = z(1);
   zm = z(2);
   
   % Set grid spacing to 50 m
   dz = 50;

   z = zl:dz:zm;
   if z(end) ~= zm; z(end+1) = zm; end

end
   

% Initialise pressure vector
if length(p) == 1
   
   calc_pressure = 1;
   p = repmat(p,[1 length(z)]);
    
else
    
   if length(p) ~= length(z); error('Pressure not same length as height'); end
   calc_pressure = 0;
   
end
   

% Initialise temperature and pressure vectors
T = zeros(size(z));
RH = zeros(size(z));

T(1) = Tl;


%% Integrate lapse rate equation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function_dTpdz = @(z,Tp) [-calculate_ZBP_lapse_rate(Tp(1),exp(Tp(2)),epsilon,PE); -(c.g)./(c.Rd.*Tp(1))];

for i = 1:length(z)

    % Get the relative humidity
    [Gamma,Gamma_m,RH(i)] = calculate_ZBP_lapse_rate(T(i),p(i),epsilon,PE);

    if i < length(z)
        
      % Use Runge-Kutta 4th order method
      Tp_in = [T(i); log(p(i))];
      dz = z(i+1)-z(i);
      [Tp_out,~] = ODE.rk_step(function_dTpdz,Tp_in,z(i),dz,'rk4');

      % Set the variables we need
      T(i+1) = Tp_out(1);
      if calc_pressure; p(i+1) = exp(Tp_out(2)); end
    
    end

  
end



    

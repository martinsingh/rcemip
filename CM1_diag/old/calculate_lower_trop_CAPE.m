function [CAPE_lower_trop,dT_zm,dT_dry] = calculate_lower_trop_CAPE(T,p,z)


%% Thermodynamics %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c = atm.load_constants;


%% Create vectors %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   
% Temperature and pressure vectors
Tm = zeros(size(z));


Tm(1) = T(1);

%% Integrate lapse rate equation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i = 1:length(z)
       
    [Gamma,Gamma_m,RH(i)] = calculate_ZBP_lapse_rate(T(i),p(i),0,1);
    
    if i<length(z)
      
      % Calculate zero entrainment temperature
      Tm(i+1) = Tm(i) - Gamma_m.*(z(i+1) - z(i));
      
    end
end

CAPE_lower_trop = c.g.*trapz(z,(Tm-T)./T);
dT_zm = Tm(end) - T(end);
dT_dry = Tm(end) - ( T(1) - c.g./c.cp.*(z(end) - z(1)) );
    

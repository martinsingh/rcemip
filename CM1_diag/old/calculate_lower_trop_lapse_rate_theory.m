function [Gamma_lower_trop,RH_lower_trop] = calculate_lower_trop_lapse_rate_theory(Tl,zl,pl,zm,epsilon,PE)


%% Thermodynamics %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c = atm.load_constants;


%% Create vectors %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Height vector
dz = 50;

z = zl:dz:zm;
if z(end) ~= zm; z(end+1) = zm; end
   
% Temperature and pressure vectors
T = zeros(size(z));
p = zeros(size(z));
RH = zeros(size(z));

T(1) = Tl;
p(1) = pl;

%% Integrate lapse rate equation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i = 1:length(z)

       
    [Gamma,Gamma_m,RH(i)] = calculate_ZBP_lapse_rate(T(i),p(i),epsilon,PE);
    
    if i<length(z)
      T(i+1) = T(i) - Gamma.*(z(i+1) - z(i));
      p(i+1) = p(i) - c.g.*p(i)./(c.Rd.*T(i)).*(z(i+1) - z(i));
    end
end

rho = p./c.Rd.*T;

Gamma_lower_trop = -(T(end) - T(1))./(zm-zl);
RH_lower_trop = trapz(z,rho.*RH)./trapz(z,rho);
    

function [T,RH] = calculate_temp_profile_theory(Tl,zl,pl,zm,epsilon,PE)
%
% Calculate the temperature and relative humidity profiles
% between two levels zl and zm in the lower troposphere
% based on the zero-buoyancy plume model. 
%
% Inputs:
%
%    Tl = temperature at lower level
%    zl = height of lower level
%    pl = pressure of lower level
%    zm = height at upper level
%
%    epsilon = entrainment rate
%    PE = precipitation efficiency
%

%% Thermodynamics %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c = atm.load_constants;


%% Create vectors %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Height vector
dz = 50;

z = zl:dz:zm;
if z(end) ~= zm; z(end+1) = zm; end
   
% Temperature and pressure vectors
T = zeros(size(z));
p = zeros(size(z));
RH = zeros(size(z));

T(1) = Tl;
p(1) = pl;

%% Integrate lapse rate equation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i = 1:length(z)

       
    [Gamma,Gamma_m,RH(i)] = calculate_ZBP_lapse_rate(T(i),p(i),epsilon,PE);
    
    if i<length(z)
      % Calculate ZBP temperature
      T(i+1) = T(i) - Gamma.*(z(i+1) - z(i));
      
      % Calculate zero entrainment temperature
      Tm(i+1) = Tm(i) - Gamma_m.*(z(i+1) - z(i));
      
      % Calculate pressure at next level
      p(i+1) = p(i) - c.g.*p(i)./(c.Rd.*T(i)).*(z(i+1) - z(i));
    end
end



    

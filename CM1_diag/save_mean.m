% Read and save the data from a given simulation


data_path =  '/g/data/k10/mss565/CM1_output/';

expts = {...
'RCEMIPN96_dx1000.0_SST300'...
'RCEMIPN96_dx1000.0_SST305'...
'RCEMIPN96_dx1000.0_SST295'...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow10.0_arain10.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow10.0_arain10.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow1.0_arain1.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow1.0_arain1.0_thic0.1_thcl0.1' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow1.0_arain1.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow5.0_arain5.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow5.0_arain5.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt-1_asnow10.0_arain10.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt-1_asnow1.0_arain1.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt-1_asnow3.0_arain3.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt-1_asnow5.0_arain5.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt2.0_asnow1.0_arain1.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt8.0_asnow1.0_arain1.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt8.0_asnow5.0_arain5.0_thic1.0_thcl1.0' ...
};


c = atm.load_constants('bolton');


for i = 1:length(expts)
expt = expts{i};

disp(expt)

data_loc = [data_path expt '/netcdf/'];
N = length(dir([data_loc '*.nc']))-2;

frames = [N-600+1 N];

i = 0;
for iframe = frames(1):frames(2)

   i = i+1;

   file_name = [data_loc 'cm1out_' num2str(iframe,'%06i') '.nc'];

   if i ==1
      x = ncread(file_name,'xh');
      y = ncread(file_name,'yh');
      zf = ncread(file_name,'zf').*1000;
      z = ( zf(1:end-1)+zf(2:end) )./2;
      dz = diff(zf);
      dz_mat = permute(repmat(dz,[1 length(x) length(y)]),[2 3 1]);

      time_init = ncread(file_name,'time');
      P_init = ncread(file_name,'rain'); 

      T_mean = zeros(size(z));
      p_mean = zeros(size(z));
      rho_mean = zeros(size(z));
      Trho_mean = zeros(size(z));
      qv_mean = zeros(size(z));
      qc_mean = zeros(size(z));
      qi_mean = zeros(size(z));
      qr_mean = zeros(size(z));
      qs_mean = zeros(size(z));
      qg_mean = zeros(size(z));
      rv_mean = zeros(size(z));
      rc_mean = zeros(size(z));
      ri_mean = zeros(size(z));
      rr_mean = zeros(size(z));
      rs_mean = zeros(size(z));
      rg_mean = zeros(size(z));
      RH_mean = zeros(size(z));
      condg_mean = zeros(size(z));
      condn_mean = zeros(size(z));

      qv_path_mean = 0.;
      qc_path_mean = 0.;
      qr_path_mean = 0.;
      qi_path_mean = 0.;
      qs_path_mean = 0.;
      qg_path_mean = 0.;

   end

  time = ncread(file_name,'time');
  if mod(time./86400,1)==0
     disp(['day ' num2str(time./86400)])
  end

  % Thermodynamic variables
  theta = ncread(file_name,'th');
  pi = ncread(file_name,'pi');
  p = ncread(file_name,'prs');
  rhod = ncread(file_name,'rho');

  % Water mixing ratios
  rv = ncread(file_name,'qv');
  rc = ncread(file_name,'qc');
  ri = ncread(file_name,'qi');

  rr = ncread(file_name,'qr');
  rs = ncread(file_name,'qs');
  rg = ncread(file_name,'qg');
  

  % micrphysical tendency of water vapour 
  qvmicten = ncread(file_name,'qvmicten');


  % Calculate the variables we want

  % Total water mixing ratio
  rt = rv+rc+ri+rr+rs+rg;

  % Temperature
  T = theta.*pi;

  % Density temperature
  Trho = T.*(1 + rv./c.eps)./(1 + rt);
 
  % Vapour pressure
  e = rhod.*rv.*c.Rv.*T;

  % Saturation vapour pressure
  esat = atm.e_sat(T,'bolton');

  % Relative humdity
  RH = e./esat;

  % Mass fractions
  qv = rv./(1+rt);
  qc = rc./(1+rt);
  qi = ri./(1+rt);
  qr = rr./(1+rt);
  qs = rs./(1+rt);
  qg = rg./(1+rt);

  qt = qv+qc+qi+qr+qs+qg;

  % Gross condensation rate
  cond_gross = qvmicten.*(qvmicten<0);

  % Net condensation rate
  cond_net = qvmicten;


  % Calculate the total water amounts
  qv_path = sum( rv.*rhod.*dz_mat,3);
  qc_path = sum( rc.*rhod.*dz_mat,3);
  qi_path = sum( ri.*rhod.*dz_mat,3);
  qr_path = sum( rr.*rhod.*dz_mat,3);
  qs_path = sum( rs.*rhod.*dz_mat,3);
  qg_path = sum( rg.*rhod.*dz_mat,3);


  % Now average horizontally
  T_mean    = T_mean    + squeeze(mean(mean(  T     )));
  p_mean    = p_mean    + squeeze(mean(mean(  p     )));
  Trho_mean = Trho_mean + squeeze(mean(mean(  Trho  )));
  qv_mean   = qv_mean   + squeeze(mean(mean(  qv    )));
  qc_mean   = qc_mean   + squeeze(mean(mean(  qc    )));
  qi_mean   = qi_mean   + squeeze(mean(mean(  qi    )));
  qr_mean   = qr_mean   + squeeze(mean(mean(  qr    )));
  qs_mean   = qs_mean   + squeeze(mean(mean(  qs    )));
  qg_mean   = qg_mean   + squeeze(mean(mean(  qg    )));
  RH_mean   = RH_mean   + squeeze(mean(mean(  RH    )));

  rv_mean   = rv_mean   + squeeze(mean(mean(  rv    )));
  rc_mean   = rc_mean   + squeeze(mean(mean(  rc    )));
  ri_mean   = ri_mean   + squeeze(mean(mean(  ri    )));
  rr_mean   = rr_mean   + squeeze(mean(mean(  rr    )));
  rs_mean   = rs_mean   + squeeze(mean(mean(  rs    )));
  rg_mean   = rg_mean   + squeeze(mean(mean(  rg    )));

  qv_path_mean   = qv_path_mean   + squeeze(mean(mean(  qv_path    )));
  qc_path_mean   = qc_path_mean   + squeeze(mean(mean(  qc_path    )));
  qi_path_mean   = qi_path_mean   + squeeze(mean(mean(  qi_path    )));
  qr_path_mean   = qr_path_mean   + squeeze(mean(mean(  qr_path    )));
  qs_path_mean   = qs_path_mean   + squeeze(mean(mean(  qs_path    )));
  qg_path_mean   = qg_path_mean   + squeeze(mean(mean(  qg_path    )));




 
  condg_mean   = condg_mean   + squeeze(mean(mean(  cond_gross.*rhod    )));
  condn_mean   = condn_mean   + squeeze(mean(mean(  cond_net.*rhod    )));

  rho_mean  = rho_mean  + squeeze(mean(mean(  rhod.*(1+rt)   )));




end

P_end = ncread(file_name,'rain');

P_mean = squeeze(mean(mean(P_end-P_init)))./(time - time_init).*10;

T_mean = T_mean./i;
p_mean = p_mean./i;
Trho_mean = Trho_mean./i;
qv_mean = qv_mean./i;
qc_mean = qc_mean./i;
qi_mean = qi_mean./i;
qr_mean = qr_mean./i;
qs_mean = qs_mean./i;
qg_mean = qg_mean./i;

rv_mean = rv_mean./i;
rc_mean = rc_mean./i;
ri_mean = ri_mean./i;
rr_mean = rr_mean./i;
rs_mean = rs_mean./i;
rg_mean = rg_mean./i;

RH_mean = RH_mean./i;
condg_mean = condg_mean./i;
condn_mean = condn_mean./i;

qv_path_mean   = qv_path_mean./i;
qc_path_mean   = qc_path_mean./i;
qi_path_mean   = qi_path_mean./i;
qr_path_mean   = qr_path_mean./i;
qs_path_mean   = qs_path_mean./i;
qg_path_mean   = qg_path_mean./i;



P_mean_bud = -sum(condn_mean.*dz);
P_gross  = -sum(condg_mean.*dz);


save(['./mat_data/' expt '_mean.mat'],'z','zf','dz','T_mean','Trho_mean','p_mean','qv_mean','qc_mean','qi_mean','qr_mean','qs_mean','qg_mean','rv_mean','rc_mean','ri_mean','rr_mean','rs_mean','rg_mean','RH_mean','rho_mean','condg_mean','condn_mean','P_mean','P_mean_bud','P_gross','qv_path_mean','qc_path_mean','qi_path_mean','qr_path_mean','qs_path_mean','qg_path_mean')

end

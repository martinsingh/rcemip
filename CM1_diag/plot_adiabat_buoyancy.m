function plot_adiabat_buoyancy(varargin)

% Defaults
model_name = 'CM1-LES';
sim_type = 'small';

% Parse inputs
if nargin >= 1;  model_name = varargin{1}; end
if nargin >= 2;  sim_type = varargin{2}; end
if nargin >= 3; error('Too many inputs'); end

SSTs = [295 300 305];


a = fig.bfig(32,12)


for i = 1:length(SSTs)

   % Load the model profiles
   [z,T,p,Tv,qv,RH,model_name] =load_model(model_name,SSTs(i),sim_type);

    % Calculate the lapse rate of virtual temperature of the simulation
    dTvdz = diff(T)./diff(z);
    zmid = (z(2:end)+z(1:end-1))./2;

   % Calculate the freezing level
   c = atm.load_constants;
   zF = interp1(T(1:30),z(1:30),c.T0);
  


   %% Calculate the adiabat 
   
   % The adiabat
   type = 'default';
   ice = 1;
   deltaT = 40;
   
   % calculate parcel properties

   % Set parcel at saturation at a particular level
   zB = 1000;
   [~,I] = min(abs(z-zB));
   T_par = T(I);
   p_par = p(I);
   r_par = atm.r_sat(T_par,p_par,type,ice,deltaT);

   % Set parcel as lowest model level
   %T_par = T(1);
   %p_par = p(1);
   %r_par = qv(1)./(1-qv(1));

   % Calculate both pseudoadiabatic and no fallout
   [T_psuedo,rv_psuedo,rl_psuedo,ri_psuedo,T_rho_psuedo,T_LCL,p_LCL,p_out]= atm.calculate_adiabat(T_par,r_par,p_par,p,1,type,ice,deltaT);
   [T_adiabat,rv_adiabat,rl_adiabat,ri_adiabat,T_rho_adiabat,T_LCL,p_LCL,p_out]= atm.calculate_adiabat(T_par,r_par,p_par,p,0,type,ice,deltaT);


   figure(a)
   subplot(132)
   plot(T_rho_adiabat-Tv,z./1000,'r');
   hold on
   plot(T_rho_adiabat-Tv,z./1000,'color',1-i.*[0.22 0.22 0]);
   set(gca,'xlim',[-8 8],'ylim',[0 10])
   xlabel('\delta T_\rho (K)')
   title('(b) adiabatic')

   subplot(133)
   %plot(T_rho_adiabat-Tv,z./1000,'r');
   hold on
   plot(T_rho_psuedo-Tv,z./1000,'color',1-i.*[0.22 0.22 0]);
   set(gca,'xlim',[-8 8],'ylim',[0 10])
   plot([-10 10],[zF zF]./1000,'color',1-i.*[0.22 0.22 0])

   xlabel('\delta T_\rho (K)')
   title('(c) pseudoadiabatic')

   subplot(131)
   plot(Tv,z./1000,'-','color',1-i.*[0.22 0.22 0]);
   hold on
   plot(T_rho_psuedo,z./1000,'--','color',1-i.*[0.22 0.22 0]);
   set(gca,'ylim',[0 18])
   xlabel('T_\rho (K)')
   title('(a) absolute temperature')
end



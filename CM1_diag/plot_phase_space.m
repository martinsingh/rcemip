%
% Plot a phase space to compare simulated humidity and temperature profiles
% to those produced from theory. Have a few different options for the key
% variables
%

%% Options %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Thermodynamic variable choice
thermo_var_type = 'LT_CAPE';
%thermo_var_type = 'MT_dT';
%thermo_var_type = 'MT_dT_norm';

% Definition of lower troposphere                            
zl = 1000;
zm = 5000;

% Qantities to initialise the ZBP solutions
Tl_ZBP = 285;                   % temperature at zl (K)
pl_ZBP = 89400;                 % pressure at zl (pa)

       
% Simulations to plot
expts = {...
'RCEMIPN96_dx1000.0_SST300'...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow10.0_arain10.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow10.0_arain10.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow1.0_arain1.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow1.0_arain1.0_thic0.1_thcl0.1' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow1.0_arain1.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow5.0_arain5.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt15.0_asnow5.0_arain5.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt-1_asnow10.0_arain10.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt-1_asnow1.0_arain1.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt-1_asnow3.0_arain3.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt-1_asnow5.0_arain5.0_thic0.01_thcl0.01' ...
'RCEMIPN96_dx1000.0_SST300_vt2.0_asnow1.0_arain1.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt8.0_asnow1.0_arain1.0_thic1.0_thcl1.0' ...
'RCEMIPN96_dx1000.0_SST300_vt8.0_asnow5.0_arain5.0_thic1.0_thcl1.0' ...
};



%% Calculate the theoretical values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       
c = atm.load_constants;

% Plot the solution for a range of precipitation efficiencies and entrainment rates

% HI res - ths takes a few minutes to calculate
%PE_ZBP = [0:0.01:1];
%epsilon_ZBP = [0:0.01:0.05 0.06:0.02:2 2.1:0.1:2.5 2.75 3 3.5 4 5 6 7 8].*1e-3;

% Low-res - use this for testing
PE_ZBP = [0:0.04:1];
epsilon_ZBP = [0:0.02:0.1 0.2:0.1:2 2.1 2.5 3 4 5 6 7 8].*1e-3;

% make some matrices
PE_ZBP_mat = repmat(PE_ZBP',[1,length(epsilon_ZBP)]);
epsilon_ZBP_mat = repmat(epsilon_ZBP,[length(PE_ZBP),1]);

% Initialize the matrices
thermo_var_ZBP = nan(length(PE_ZBP),length(epsilon_ZBP));
hum_var_ZBP = nan(length(PE_ZBP),length(epsilon_ZBP));


% Calculate this in a loop because I am lazy
for i = 1:length(PE_ZBP)
    
    disp(['Theory: ' num2str(i) ' of ' num2str(length(PE_ZBP))])
    
    for j = 1:length(epsilon_ZBP)

        % ZBP solution for profiles
        [T,p,z,RH] = calculate_ZBP(Tl_ZBP,[zl zm],pl_ZBP,epsilon_ZBP(j),PE_ZBP(i));

        % Zero entrainment case to approximate moist adiabat
        Tm = calculate_ZBP(Tl_ZBP,z,p,0,1);

        
        % Calculate lower-tropoospheric relative humidity
        rho = p./(c.Rd.*T);
        hum_var_ZBP(i,j) = trapz(z,rho.*RH)./trapz(z,rho);
        
        if strcmp(thermo_var_type,'LT_CAPE')
            
            % Integral of buoyancy over lower troposphere
            thermo_var_ZBP(i,j) = c.g.*trapz(z,(Tm-T)./T);           
            
        elseif contains(thermo_var_type,'MT_dT')
            
            % Temperature difference to moist adiabat at zm
            dT = Tm(end) - T(end);
            
            % Temperature difference between moist and dry adiabat
            dT_dry = Tm(end) - ( Tm(1) - c.g./c.cp.*(zm-zl) );
            
            %Integral of buoyancy over lower troposphere
            thermo_var_ZBP(i,j) = dT;           
            
            if strcmp(thermo_var_type,'MT_dT_norm')
                thermo_var_ZBP(i,j) = dT./dT_dry;
            end
            
            
        else
            
            error('unknown target variable')
            
        end
        

        
    end
end



%% Calculate the simulated values of the variables of interest


% Initialise variables
PE = zeros(length(expts),1);
PE_proxy = zeros(length(expts),1);
PE_proxy2 = zeros(length(expts),1);

Tl = zeros(length(expts),1);
pl = zeros(length(expts),1);

T_LCL = zeros(length(expts),1);
p_LCL = zeros(length(expts),1);

CAPE = zeros(length(expts),1);
p_LNB = zeros(length(expts),1);
T_rho_LNB = zeros(length(expts),1);


hum_var = zeros(length(expts),1);
thermo_var = zeros(length(expts),1);

epsilon_theory = zeros(length(expts),1);
PE_theory = zeros(length(expts),1);

CAPE_theory = zeros(length(expts),1);
CAPE_theory_fixed_depth = zeros(length(expts),1);
CAPE_theory_fixed_PE = zeros(length(expts),1);
CAPE_theory_fixed_ent = zeros(length(expts),1);



for i =1:length(expts)

   disp(['Simulations: ' num2str(i) ' of ' num2str(length(expts))])

    
   % Load mean data (saved with save_mean.m)
   load(['./mat_data/' expts{i} '_mean.mat'])

   % Calculate preciptiation efficiency
   PE(i) = P_mean./P_gross;

   % Calculate proxy preciptiation efficiency
   PE_proxy(i) = P_mean./(qc_path_mean+qi_path_mean+qs_path_mean+qr_path_mean+qg_path_mean);
   PE_proxy2(i) = P_mean./(qc_path_mean+qi_path_mean);
   
   % Calculate an adiabatic parcel ascent
   %[T_par,r_par,rl_par,ri_par,T_rho_par] = atm.calculate_adiabat(T_mean(1),rv_mean(1),p_mean(1),p_mean,1);
   %dTv_adiabatic(i,:) = T_rho_par - Trho_mean;
   
   % Calculate the LCL
   [T_LCL(i),p_LCL(i)] = atm.calculate_LCL(T_mean(1),rv_mean(1),p_mean(1));

   % Calculate CAPE
   [CAPE(i),p_LNB(i),T_rho_LNB(i)] = calculate_CAPE(T_mean(1),rv_mean(1),p_mean(1),Trho_mean,p_mean);
   

   % Calculate lower-troposphere
   [~,Im] = min(abs(z-zm));
   [~,Il] = min(abs(z-zl));
   I = false(length(z),1); I(Il:Im) = true;
      
 
   % Calculate lower-tropospheric humidity
   hum_var(i) = sum(RH_mean.*rho_mean.* I.*dz)./sum(rho_mean.* I.*dz);

   % Calculate quantities at bottom level
   Tl(i) = T_mean(Il);
   pl(i) = p_mean(Il);
   
   % Zero entrainment case to approximate moist adiabat
   Tm = calculate_ZBP(Tl(i),z(I),p_mean(I),0,1);
   
   if strcmp(thermo_var_type,'LT_CAPE')
            
       % Integral of buoyancy over lower troposphere
       thermo_var(i) = c.g.*trapz(z(I),(Tm-T_mean(I))./T_mean(I));          
            
   elseif contains(thermo_var_type,'MT_dT')
            
         % Temperature difference to moist adiabat at zm
         dT = Tm(end) - T_mean(Im);
            
         % Temperature difference between moist and dry adiabat
         dT_dry = Tm(end) - ( Tm(1) - c.g./c.cp.*(zm-zl) );
            
         %Integral of buoyancy over lower troposphere
         thermo_var(i) = dT;           
            
         if strcmp(thermo_var_type,'MT_dT_norm')
             thermo_var(i) = dT./dT_dry;
         end
            
            
   end
   
   % Calculate the theoretical PE and entrainment based on the thermodynamic variables
   

   % Calculate contours of CAPE and RH for the simulated values and
   % seee where they intersect
   % Also tried MATLAB's "scattered interpolant" function, but this
   % produced inaccurate results for the entrainment. Not sre why
   
   % The contours
   C1 = contourc(epsilon_ZBP,PE_ZBP,thermo_var_ZBP,[thermo_var(i) thermo_var(i)]);
   C2 = contourc(epsilon_ZBP,PE_ZBP,hum_var_ZBP,[hum_var(i) hum_var(i)]);
   
   % Find intersection
   [epsilon_theory(i),PE_theory(i)] = polyxpoly(C1(1,2:end),C1(2,2:end),C2(1,2:end),C2(2,2:end));
   
   
   % Calculate theoretical CAPE based on these values
   CAPE_theory(i) = calculate_CAPE_theory(T_LCL(i),T_rho_LNB(i),p_LCL(i),epsilon_theory(i),PE_theory(i));
   

end

for i = 1:length(expts)
    
   CAPE_theory_fixed_depth(i) = calculate_CAPE_theory(mean(T_LCL),mean(T_rho_LNB),p_LCL(i),epsilon_theory(i),PE_theory(i));
   CAPE_theory_fixed_PE(i) = calculate_CAPE_theory(mean(T_LCL),mean(T_rho_LNB),p_LCL(i),epsilon_theory(i),mean(PE_theory));
   CAPE_theory_fixed_ent(i) = calculate_CAPE_theory(mean(T_LCL),mean(T_rho_LNB),p_LCL(i),mean(epsilon_theory),PE_theory(i));
   
end



%% Plot the phase space

% Which values should we plot
PE_plot = [0.2 0.4 0.6 0.8 1];
eps_plot = [0.0 0.1 0.2 0.4 0.8 1.6].*1e-3;

% Which values do we label
PE_lab = [0.2 0.6  1];
eps_lab = [0.0 0.1 0.2 0.4 0.8 1.6].*1e-3;


% Axis stuff
x_lims = [0.5 1];
x_labs = ('lower-tropospheric relative humidity');
eps_pos = [0.7 0.7 0.7 0.7 0.9 0.95];

if strcmp(thermo_var_type,'LT_CAPE')
    
    y_lims = [0 600];
    y_labs = 'lower-tropospheric CAPE (J kg^{-1})';
    PE_pos = [520 430 570];
    

elseif strcmp(thermo_var_type,'MT_dT')
    y_lims = [0 8];
    y_labs = 'mid-tropospheric buoyancy (K)';
    PE_pos = [7.3 6 7.5];
    
elseif strcmp(thermo_var_type,'MT_dT_norm')
    y_lims = [0 0.5];
    y_labs = 'normalised lapse rate';
    PE_pos = [0.4 0.4 0.4];
    
    
end



% Make the figure
fig.bfig(16,12)

set(gca,'ylim',y_lims)
set(gca,'xlim',x_lims)
hold on

% Plot the lines of constant precipitation efficiency
for i = 1:length(PE_plot)

    % Find the precipitation efficiency we want
    I = find(PE_ZBP==PE_plot(i));

    % Plot the line
    pp = plot(hum_var_ZBP(I,:),thermo_var_ZBP(I,:),'k');

    % Label the line
    if ismember(PE_plot(i),PE_lab)
        j = find(PE_lab==PE_plot(i));
        ll = fig.inline_label(pp,['PE = ' num2str(PE_plot(i))],[],PE_pos(j));
    end

end


% Plot the lines of constant entrainment rate
for i = 1:length(eps_plot)

    % Find the precipitation efficiency we want
    I = find(epsilon_ZBP==eps_plot(i));

    % Plot the line
    pp = plot(hum_var_ZBP(:,I),thermo_var_ZBP(:,I),'color',[0.5 0.5 0.5]);

    % Label the line
    if ismember(eps_plot(i),eps_lab)
       j = find(eps_lab==eps_plot(i));
       ll = fig.inline_label(pp,['\epsilon = ' num2str(eps_plot(i).*1000)],eps_pos(j),[],'color',[0.5 0.5 0.5]);
    end

end


scatter(hum_var,thermo_var,20,PE,'filled')

cc = colorbar;
ylabel(cc,'precip. efficiency')

set(gca,'xlim',x_lims,'ylim',y_lims)

xlabel(x_labs)
ylabel(y_labs)
box off


print -dpdf ../Figures/phase_space_altered_mic.pdf






%% Plot PE versus proxy and theory

fig.bfig(8,17)
subplot(311)
plot(PE,PE_theory,'o')
hold on
plot([0 1],[0 1],'-','color',[0.5 0.5 0.5])

pp = polyfit(PE,PE_theory,1);
Rsquared = corr(PE,PE_theory).^2;
x = 0:0.1:1;
plot(x,pp(1).*x+pp(2),'k--')
text(0.05,0.7,['R^2 = ' num2str(Rsquared)])
%xlabel('precipitation efficiency')
ylabel('theoretical precip. efficiency')
set(gca,'xlim',[0 0.6],'ylim',[0 0.75])

box off

subplot(312)

plot(PE,PE_proxy,'o')
hold on

set(gca,'xlim',[0 0.6],'ylim',[0 1.7e-3])

pp = polyfit(PE,PE_proxy,1);
Rsquared = corr(PE,PE_proxy).^2;
x = 0:0.1:1;
plot(x,pp(1).*x+pp(2),'k--')
text(0.05,1.3e-3,['R^2 = ' num2str(Rsquared)])

set(gca,'xlim',[0 0.6],'ylim',[0 1.7e-3])

%xlabel('precipitation efficiency')
ylabel('proxy precip. efficiency')

box off


subplot(313)

plot(PE,hum_var,'o')
hold on



pp = polyfit(PE,hum_var,1);
Rsquared = corr(PE,hum_var).^2;
x = 0:0.1:1;
plot(x,pp(1).*x+pp(2),'k--')
text(0.05,0.65,['R^2 = ' num2str(Rsquared)])

set(gca,'xlim',[0 0.6],'ylim',[0.6 1])

xlabel('precipitation efficiency')
ylabel('tropospheric RH')


box off

print -dpdf ../Figures/precip_efficiency.pdf


%% Plot CAPE and compare to the different cases
fig.bfig(10,18)

L = 0.18;
B = [0.78 0.55 0.32 0.09];
W = 0.77;
H  = 0.18;

axes('position',[L B(1) W H])
plot(CAPE,CAPE_theory,'o')
hold on
pp = polyfit(CAPE,CAPE_theory,1);
Rsquared = corr(CAPE,CAPE_theory).^2;
plot(CAPE,pp(1).*CAPE+pp(2),'k--')
text(100,900,['R^2 = ' num2str(Rsquared,3)])
text(100,300,['\sigma = ' num2str(var(CAPE_theory).^0.5,3)])
set(gca,'xlim',[0 2000],'ylim',[0 4000])
box off
set(gca,'xticklabel','')
text(30,4200,'a) all factors')

axes('position',[L B(2) W H])
plot(CAPE,CAPE_theory_fixed_depth,'o')
hold on
pp = polyfit(CAPE,CAPE_theory_fixed_depth,1);
Rsquared = corr(CAPE,CAPE_theory_fixed_depth).^2;
plot(CAPE,pp(1).*CAPE+pp(2),'k--')
text(100,900,['R^2 = ' num2str(Rsquared,3)])
text(100,300,['\sigma = ' num2str(var(CAPE_theory_fixed_depth).^0.5,3)])
set(gca,'xlim',[0 2000],'ylim',[0 4000])
box off
yy = ylabel('Theoretical CAPE (J kg^{-1})');
yypos = get(yy,'position');
set(yy,'position',yypos-[0 2000 0]);
set(gca,'xticklabel','')
text(30,4200,'b) fixed depth')

axes('position',[L B(3) W H])
plot(CAPE,CAPE_theory_fixed_PE,'o')
hold on
pp = polyfit(CAPE,CAPE_theory_fixed_PE,1);
Rsquared = corr(CAPE,CAPE_theory_fixed_PE).^2;
plot(CAPE,pp(1).*CAPE+pp(2),'k--')
text(100,900,['R^2 = ' num2str(Rsquared,3)])
text(100,300,['\sigma = ' num2str(var(CAPE_theory_fixed_PE).^0.5,3)])
set(gca,'xlim',[0 2000],'ylim',[0 4000])
box off
%ylabel('Theoretical CAPE (J kg^{-1})')
set(gca,'xticklabel','')
text(30,4200,'c) fixed precip. efficiency')

axes('position',[L B(4) W H])
plot(CAPE,CAPE_theory_fixed_ent,'o')
hold on
pp = polyfit(CAPE,CAPE_theory_fixed_ent,1);
Rsquared = corr(CAPE,CAPE_theory_fixed_ent).^2;
plot(CAPE,pp(1).*CAPE+pp(2),'k--')
text(100,900,['R^2 = ' num2str(Rsquared,3)])
text(100,300,['\sigma = ' num2str(var(CAPE_theory_fixed_ent).^0.5,3)])
set(gca,'xlim',[0 2000],'ylim',[0 4000])
box off
%ylabel('Theoretical CAPE (J kg^{-1})')
xlabel('simulated (CAPE J kg^{-1})')

text(30,4200,'d) fixed entrainment')


print -dpdf ../Figures/CAPE.pdf
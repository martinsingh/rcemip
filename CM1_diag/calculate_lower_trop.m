function [Il,Im,zl,zm,Tl,pl] = calculate_lower_trop(z_lower,z_mid,z,T,p)
%
% Get the lower tropospheric z boundaries and temperature and pressure at
% the lower boundary based on some model profiles.
%
% Simple calculation, just put it in a function to make sure it is
% consistent everywhere

    % Lower boundary
    [~,Il] = min(abs(z-z_lower));
    Tl = T(Il);
    pl = p(Il);
    zl = z(Il);

    % Upper boundary
    [~,Im] = min(abs(z-z_mid));
    zm = z(Im);
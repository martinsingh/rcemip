
% surface temp of related RCE simulation
SSTs = [295 300 305];


for iT = 1:length(SSTs)
    
    SST0 = SSTs(iT);

    disp(['SST = ' num2str(SST0) ' K'])
    
%% Load the RCE output %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Location of RCE output
outloc = '/short/wa6/mss565/SAM_output/RCEMIP/';

% Load the stats file for the RCE case
ncutil.ncload([outloc 'RCEMIP_SST' num2str(SST0) '_96x96x146-1000m-8s' '/OUT_STAT/' 'RCEMIP_SST' num2str(SST0) '_96x96x146-1000m-8s.nc'])

% Find days 50-100
I = time>50 & time<=100;



% Get the average of the days 50-100
 
qv_mean = mean(QV(:,I),2);
T_mean = mean(TABS(:,I),2);
th_mean = mean(THETA(:,I),2);
cld_mean = mean(CLD(:,I),2);
p_mean = mean(PRES(:,I),2);
ps_mean = mean(Ps(I));

%% Make the required input files %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%% qvprofile file %%%

fid = fopen(['./soundings/qvprof_RCE_LES_SST' num2str(SST0)],'w');

fprintf(fid,'%s\n',' z[m]    qvprof[g/kg]');

for i = 1:length(z)
   fprintf(fid,'%8.3f %10.7f\n',z(i),qv_mean(i));
end

fclose(fid);


%% Input sounding file

fid = fopen(['./soundings/snd_RCE_LES_SST' num2str(SST0)],'w');

fprintf(fid,'%s\n','  z[m]    p[mb]   tp[K]   q[g/kg]         u[m/s]  v[m/s]');

fprintf(fid,'%d %d %10.7f\n',0,length(z),ps_mean);
for i = 1:length(z)
   fprintf(fid,'%8.3f %10.7f %10.7f %10.7f %10.7f %10.7f\n',z(i),p_mean(i),th_mean(i),qv_mean(i),0,0);
end

fprintf(fid,'%d %d %10.7f\n',1000,length(z),ps_mean);
for i = 1:length(z)
   fprintf(fid,'%8.3f %10.7f %10.7f %10.7f %10.7f %10.7f\n',z(i),p_mean(i),th_mean(i),qv_mean(i),0,0);
end


fclose(fid);



end



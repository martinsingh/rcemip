function [T,q,p] = rcemip_on_z(z,SST)
% function [T,q,p] = rcemip_on_z(z,SST)
%
% Inputs:
% z: array of heights (low to high, m)
% SST: sea surface temperature (K)
%
% Outputs:
% T: temperature (K)
% q: specific humidity (g/g)
% p: pressure (Pa)


%% Constants
g = 9.79764; %m/s^2
Rd = 287.04; %J/kgK

%% Parameters
p0 = 101480; %Pa surface pressure
qt = 10^(-14); %g/g specific humidity at tropopause. Prior to 02Sep2020, erroneously specified as 10^(-11)
zq1 = 4000; %m
zq2 = 7500; %m
zt = 15000; %m tropopause height
gamma = 0.0067; %K/m lapse rate

if SST == 295
    q0 = 0.01200; %g/g specific humidity at surface (adjusted from 300K value so RH near surface approx 80%)
elseif SST == 300
    q0 = 0.01865; %g/g specific humidity at surface
elseif SST == 305
    q0 = 0.02400; %g/g specific humidity at surface (adjusted from 300K value so RH near surface approx 80%)
end
T0 = SST - 0; %surface air temperature adjusted to be 0K less than SST

%% Virtual Temperature at surface and tropopause
Tv0 = T0*(1 + 0.608*q0); %virtual temperature at surface
Tvt = Tv0 - gamma*zt; %virtual temperature at tropopause z=zt

%% Pressure
pt = p0*(Tvt./Tv0).^(g/(Rd*gamma)); %pressure at tropopause z=zt
p = p0*((Tv0-gamma*z)/Tv0).^(g/(Rd*gamma)); %0 <= z <= zt
p(z>zt) = pt*exp(-g*(z(z>zt)-zt)/(Rd*Tvt)); %z > zt

%% Specific humidity
q = q0*exp(-z/zq1).*exp(-(z/zq2).^2);
q(z>zt) = qt; %z > zt

%% Temperature
%Virtual Temperature
Tv = Tv0 - gamma*z; % 0 <= z <= zt
Tv(z>zt) = Tvt; %z > zt

%Absolute Temperature at all heights
T = Tv./(1 + 0.608.*q);

end
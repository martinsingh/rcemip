
% surface temp of related RCE simulation
SSTs = [295 300 305];

% z_top values for the three SSTs
z_tops = [11 12.5 14].*1000;

% maximum in upward velocity
w_maxs = [-0.01 -0.009 -0.008 -0.007 -0.006 -0.005 -0.002 0.005 0.01 0.02 0.03 0.04 0.05 0.08];


for iT = 1:length(SSTs)
    
    SST0 = SSTs(iT);
    z_top = z_tops(iT);

    disp(['SST = ' num2str(SST0) ' K, z_top = ' num2str(z_top/1000) ' km'])
    
%% Load the RCE output %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Location of RCE output
outloc = '/g/data/wa6/mss565/SAM_output/RCEMIP/';

% Load the stats file for the RCE case
ncutil.ncload([outloc 'RCEMIP_SST' num2str(SST0) '_96x96x74-1000m-8s' '/OUT_STAT/' 'RCEMIP_SST' num2str(SST0) '_96x96x74-1000m-8s.nc'])

% Find days 50-100
I = time>50 & time<=100;



% Get the average of the days 50-100
 
qv_mean = mean(QV(:,I),2);
T_mean = mean(TABS(:,I),2);
th_mean = mean(THETA(:,I),2);
cld_mean = mean(CLD(:,I),2);
p_mean = mean(PRES(:,I),2);
ps_mean = mean(Ps(I));

%% Calculate the forcing %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for iw = 1:length(w_maxs)

    w_max = w_maxs(iw);
    
% Create w forcing
w_forcing = w_maxs(iw).*sin(pi().*z./(z_top));
w_forcing(z>z_top) = 0;

% Create a dummy vector
dummy = -999.*ones(size(z));
zz = zeros(size(z));

%% Make the required input files %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%% Forcing file %%%

fid = fopen(['./forcings/lsf_wmax' num2str(w_max) '_SST' num2str(SST0) '_ztop' num2str(z_top/1000)],'w');


formatSpec = '%8.3f %8.0f %8.3f %8.3f %8.3f %8.3f %10.7f\n';


fprintf(fid,'%s\n',' z[m] p[mb]  tls[K/s] qls[kg/kg/s] uls  vls  wls[m/s]');

fprintf(fid,'%s\n','0.,  74   1015. day, nlevels, pres0');

for i = 1:length(z)
   fprintf(fid,formatSpec,z(i),-999,0,0,0,0,w_forcing(i));
end

fprintf(fid,'%s\n','1000.,  74   1015. day, nlevels, pres0');
for i = 1:length(z)
   fprintf(fid,formatSpec,z(i),-999,0,0,0,0,w_forcing(i));
end


fclose(fid);


end


%% qvprofile file %%%

fid = fopen(['./soundings/qvprof_SST' num2str(SST0)],'w');

fprintf(fid,'%s\n',' z[m]    qvprof[g/kg]');

for i = 1:length(z)
   fprintf(fid,'%8.3f %10.7f\n',z(i),qv_mean(i));
end

fclose(fid);


%% Input sounding file

fid = fopen(['./soundings/snd_RCE_SST' num2str(SST0)],'w');

fprintf(fid,'%s\n','  z[m]    p[mb]   tp[K]   q[g/kg]         u[m/s]  v[m/s]');

fprintf(fid,'%d %d %10.7f\n',0,length(z),ps_mean);
for i = 1:length(z)
   fprintf(fid,'%8.3f %10.7f %10.7f %10.7f %10.7f %10.7f\n',z(i),p_mean(i),th_mean(i),qv_mean(i),0,0);
end

fprintf(fid,'%d %d %10.7f\n',1000,length(z),ps_mean);
for i = 1:length(z)
   fprintf(fid,'%8.3f %10.7f %10.7f %10.7f %10.7f %10.7f\n',z(i),p_mean(i),th_mean(i),qv_mean(i),0,0);
end


fclose(fid);



end



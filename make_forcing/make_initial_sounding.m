%% Script to generate initial sounding from analytic expression for SAM

model = 'CM1';

%% File specifications
filename = 'snd_rcemip_anal295_v3';

%% Constants
Rd = 287.04; %J/kgK
cp = 1004.64; %J/kgK

%% Parameters
SST = 295; %K surface temperature
p0 = 101480; %Pa surface pressure

%% Altitude (include surface)
z1 = [0 37 112 194 288 395 520 667 843 1062 1331 1664 2055 2505 3000];
z2 = 3500:500:33000;
z = cat(2,z1,z2);
nzm = length(z)-1; %number of levels = 74

%% Compute T, q, p from analytic expressions
[T,q,p] = rcemip_on_z(z,SST);

% Take surf values
Ts = T(1);
qs = q(1);
ps = p(1);

% Remove surface from arrays
T = T(2:end);
q = q(2:end);
p = p(2:end);
z = z(2:end);

% Calculate interface points
zf = zeros(nzm+1,1);
for i = 1:length(zf)-1
    zf(i+1) = 2.*z(i) - zf(i);
end

%% Potential Temperature
theta = T.*(1000./(p/100)).^(Rd/cp);

%% Winds
u = zeros(nzm,1);
v = zeros(nzm,1);

%% Prepare sounding
if strcmp(model,'SAM')
  snd = [z' p'/100 theta' q'*1000 u v];
elseif strcmp(model,'CM1')
  snd = [z' theta' q'*1000 u v];
else
    error('Unknwon model')
end

snd = double(snd);
   
   
format long g

dayi = 0;
dayf = 1000;
%header 
head1='z[m]';
head2='p[mb]';
head3='tp[K]';
head4='q[g/kg]';
head5='u[m/s]';
head6='v[m/s]';



% Write to file
fid=fopen(filename,'w'); %open file

if strcmp(model,'SAM')
fprintf(fid,'\t %s', head1, head2, head3, head4, head5, head6); %write header
fprintf(fid,'\n%1.0f', dayi); %write day
fprintf(fid,'\t%2i', nzm); %write nlevels
fprintf(fid,'\t%5.1f', p0/100); %write surface pressure
fprintf(fid,'\n\t%8.4f %8.4f %8.4f %8.4f %8.4f %8.4f',snd'); %write snd
fprintf(fid,'\n%1.0f', dayf); %write day
fprintf(fid,'\t%2i', nzm); %write nlevels
fprintf(fid,'\t%5.1f', p0/100); %write surface pressure
fprintf(fid,'\n\t%8.4f %8.4f %8.4f %8.4f %8.4f %8.4f',snd'); %write snd again

elseif strcmp(model,'CM1')
    
fprintf(fid,'\t%5.1f %5.1f %5.1f', p0/100,SST,qs.*1000); %write surface pressure
fprintf(fid,'\n\t%8.4f %8.4f %8.4f %8.4f %8.4f',snd'); %write snd

else
    error('unknown model')

end

fclose(fid); %close file




% Plot sounding
pmin = 5;
pmax= 1015;
figure;
subplot(1,2,1)
hold on
plot(T,p/100,'k','LineWidth',1)
set(gca,'FontSize',16)
set(gca,'Ydir','Reverse')
ylim([pmin pmax])
xlabel('T (K)')
ylabel('Pressure (hPa)')

subplot(1,2,2)
hold on
plot(q*1000,p/100,'k','LineWidth',1)
set(gca,'FontSize',16)
set(gca,'Ydir','Reverse')
ylim([pmin pmax])
xlabel('Specific Humidity (g/kg)')
ylabel('Pressure (hPa)')
% This script tests the derivatives produced by the function
% "calculate_CAPE_derivatives" by comparing them to a numerical
% approximation.
%


% Reference values
Tb0 = 295;
pb0 = 95000;
dT0 = 90;
epsilon0 = 0.5e-3;
PE0 = 0.5;

% Testing range
Tb = 273:0.01:310;
dT = 10:0.01:100;
epsilon = (0.1:0.01:2).*1e-3;
PE = 0.1:0.01:0.99;

%% Figure 

fig.bfig(15,15)


%% Test the derivative with respect to temperature

[CAPE,dC] = calculate_CAPE_derivatives(Tb,pb0.*ones(size(Tb)),dT0.*ones(size(Tb)),epsilon0.*ones(size(Tb)),PE0.*ones(size(Tb)));

% Numerically estimate dCAPE/dTb
dCAPE_numerical = gradient(CAPE,Tb);

subplot(2,2,1)
plot(Tb,dC(1,:)./CAPE.*100)
hold on
plot(Tb,dCAPE_numerical./CAPE.*100,'--')

xlabel('cloud base temp (K)')
ylabel('% CAPE increase with Tb (%/K)')
l = legend('analytic','numerical');
set(l,'box','off')
box off


%% Test derivative with respect to convection depth

[CAPE,dC] = calculate_CAPE_derivatives(Tb0.*ones(size(dT)),pb0.*ones(size(dT)),dT,epsilon0.*ones(size(dT)),PE0.*ones(size(dT)));

% Numerically estimate dCAPE/dTb
dCAPE_numerical = gradient(CAPE,dT);

subplot(2,2,2)
plot(dT,dC(3,:)./CAPE.*100)
hold on
plot(dT,dCAPE_numerical./CAPE.*100,'--')

xlabel('convection depth (K)')
ylabel('% CAPE increase with \Delta{}T (%/K)')
box off


%% Test derivative with respect to entrainment

[CAPE,dC] = calculate_CAPE_derivatives(Tb0.*ones(size(epsilon)),pb0.*ones(size(epsilon)),dT0.*ones(size(epsilon)),epsilon,PE0.*ones(size(epsilon)));

% Numerically estimate dCAPE/dTb
dCAPE_numerical = gradient(CAPE,epsilon);

subplot(2,2,3)
plot(epsilon.*1000,dC(4,:)./CAPE.*100./1000)
hold on
plot(epsilon.*1000,dCAPE_numerical./CAPE.*100./1000,'--')

xlabel('entrainment rate (km^{-1})')
ylabel('% CAPE increase with \epsilon (% km)')
box off


%% Test derivative with respect to entrainment

[CAPE,dC] = calculate_CAPE_derivatives(Tb0.*ones(size(PE)),pb0.*ones(size(PE)),dT0.*ones(size(PE)),epsilon0.*ones(size(PE)),PE);

% Numerically estimate dCAPE/dTb
dCAPE_numerical = gradient(CAPE,PE);

subplot(2,2,4)
plot(PE.*100,dC(5,:)./CAPE)
hold on
plot(PE.*100,dCAPE_numerical./CAPE,'--')

xlabel('precipitation efficiency (%)')
ylabel('% CAPE increase with PE (%/%)')
box off





%
% Plot the model profiles and compare them to the ZBP model
%

%% Parameters

% Calculate humidity for the lower half of the troposphere (in temperature
% coordinates)

% Calculate humidity based on 2-5 km
zl = 2000;
zm = 5000;


% CAPE calculation parameters
gamma = 0;
deltaT = 40;
ice = 1;


% Models and SSTs to use
mods = [1 2 3 4 5 7:18 20 23:28];
SSTs = [295 300 305];


%% Plots to make

plot_type = 2;

% Colors for the lines
col = [0.2 0.5 1];
colb = [0.3 0.3 0.3];

if plot_type == 1
   ff = fig.bfig(15,140);
   W = 0.4;
   set(ff,'defaultaxesfontsize',8,'defaultaxeslinewidth',0.5,'defaultlinelinewidth',0.75)

elseif plot_type == 2
   ff = fig.bfig(8,140);
   W = 0.8;
   set(ff,'defaultaxesfontsize',8,'defaultaxeslinewidth',0.5,'defaultlinelinewidth',0.75)

end


L = [0.1 0.55];
H = 0.032;
B = [0.95:-0.039:0.01];


c = atm.load_constants;

for imod = 1:length(mods)
for k = 1:length(SSTs)
    disp(num2str(mods(imod)))


    %% Load the model data
    [z,T,p,Tv,qv,RH,rho,model_name] =load_model(mods(imod),SSTs(k));


    %% Pick a parcel

    % Currently: lowest model level
    Tinit = T(1);
    pinit = p(1);
    rinit = qv(1)./(1-qv(1));


    %% Calculate the LCL
    [T_LCL(imod,k),p_LCL(imod,k)] = atm.calculate_LCL(Tinit,rinit,pinit,'default',ice,deltaT);


    %% Calculate parcel ascent

    % Lift a parcel from the lowest model level
    [T_par,r_par,rl_par,ri_par,T_rho_par] = atm.calculate_adiabat(Tinit,rinit,pinit,p,gamma,'default',ice,deltaT);

    % Calculate the parcel buoyancy (actually temperature difference)
    dTv = (T_rho_par - Tv);

    %% Find the LNB

    % Find last positive buoyancy point
    I = find(dTv>0,1,'last');

    % Interpolate to find the LNB
    p_LNB(imod,k) = interp1(dTv(I:I+1),p(I:I+1),0);
    T_LNB(imod,k) = interp1(dTv(I:I+1),T(I:I+1),0);

    % Sort pressure vector
    [p_new,p_order] = sort([p; p_LNB(imod)],'descend');

    % Sort dT
    dTv = [dTv; 0];
    dTv = dTv(p_order);

    %% Calculate CAPE

    % Integrate positive buoyancy with trapezoidal method.
    CAPE(imod,k) = -c.Rd.*trapz( log(p_new) , (dTv).*(dTv>0) );


    %% Calculate the lower-tropospheric relative humidity
    %I = z>zl & z<=zm;
    I = T<T_LCL(imod,k) & T>=(T_LNB(imod,k)+T_LCL(imod,k))/2 & p>p_LNB(imod,k);
    
    RH_lower_trop(imod,k) = trapz(z(I),rho(I).*RH(I))./trapz(z(I),rho(I));


    %% Now calculate the phase space
    ent_vec = [0:0.02:10].*1e-3;
    PE_vec = [0:0.02:1];
    [ent_mat,PE_mat] = meshgrid(ent_vec,PE_vec);

    % Use the Romps formula to calculate the CAPE for a range of ent and PE
    [CAPE_mat,RH_mat,CAPE_simple_mat] = calculate_CAPE_theory(T_LCL(imod,k),T_LNB(imod,k),p_LCL(imod,k),ent_mat,PE_mat);



    %% Now diagnose the entrainment and precipitation efficiency from the model

    % The contours
    C1 = contourc(ent_vec,PE_vec,CAPE_mat,[CAPE(imod,k) CAPE(imod,k)]);
    C2 = contourc(ent_vec,PE_vec,RH_mat,[RH_lower_trop(imod,k) RH_lower_trop(imod,k)]);

    % Find intersection
    [epsilon_diag(imod,k),PE_diag(imod,k)] = polyxpoly(C1(1,2:end),C1(2,2:end),C2(1,2:end),C2(2,2:end));

    % Test that we can recover the CAPE
    [CAPE_check(imod,k),RH_test(imod,k)] = calculate_CAPE_theory(T_LCL(imod,k),T_LNB(imod,k),p_LCL(imod,k),epsilon_diag(imod,k),PE_diag(imod,k));


    %% Calculate the theoretical buoyancy profile

    % Find the pressures we want to solve for
    Itrop = p<p_LCL(imod,k) & p>p_LNB(imod,k);
    iLCL = find(p>p_LCL(imod,k),1,'last');
    iLNB = find(p>p_LNB(imod,k),1,'last');
    p_ZBP = [p_LCL(imod,k); p(Itrop); p_LNB(imod,k)];

    % Now do a slightly weird calculation to find z
    z_LCL = z(iLCL) + c.Rd./c.g.*Tv(iLCL).*(log(p(iLCL))-log(p_LCL(imod,k)));
    z_LNB = z(iLNB) + c.Rd./c.g.*Tv(iLNB).*(log(p(iLNB))-log(p_LNB(imod,k)));
    z_ZBP = [z_LCL; z(Itrop); z_LNB];

    % Calculate ZBP model with a given set of pressures 
    % (could also calculate the pressures, probably won't make a lot of
    % difference)
    [T_ZBP,p_ZBP,z_ZBP,RH_ZBP,Tm_ZBP] = calculate_ZBP(z_ZBP,p_ZBP,T_LCL(imod,k),epsilon_diag(imod,k),PE_diag(imod,k));

    CAPE_check2(imod,k) = -c.Rd.*trapz(log(p_ZBP),(Tm_ZBP-T_ZBP));


    if plot_type ==1
      
        if k == 1
            aa = axes('position',[L(1) B(imod) W H ]);
            bb = axes('position',[L(2) B(imod) W H ]);
            w = 0.5;
        elseif k ==2
            w = 1;
        else
            w = 1.5;
        end

        % Plot temperature profiles
        axes(aa)
        
        plot([0 0],[0 1100],'color',[0.7 0.7 0.7],'linewidth',0.5)
        
        hold on
        
        plot(Tm_ZBP-T_ZBP,z_ZBP./1000,'color',colb,'linewidth',w);
        plot(T_rho_par-Tv,z./1000,'color',col,'linewidth',w);
        
        
        set(gca,'xlim',[-10 10],'ylim',[0 18])

        set(gca,'xtick',[-8 -4 0 4 8],'xticklabel','')
        set(gca,'ytick',[0 3 6 9 12 15 18])
        ylabel('height (km)')

        if imod < length(mods)
            set(gca,'xtick',[-8 -4 0 4 8]);
        else
            set(gca,'xtick',[-8 -4 0 4 8])
            xlabel('\Delta T_\rho (K)')
        end


        text(-9.5,1,model_name,'fontsize',8)

        % Plot relative humidity profiles
        axes(bb)

        plot(RH_ZBP,z_ZBP./1000,'color',colb,'linewidth',w);
        hold on
        plot(RH,z./1000,'color',col,'linewidth',w)
        
        set(gca,'xlim',[0.3 1],'ylim',[0 20])
        set(gca,'ytick',[0 3 6 9 12 15 18])

        if imod < length(mods)
            set(gca,'xtick',[0.3 0.5 0.7 0.9]);%,'xticklabel','')
        else
            set(gca,'xtick',[0.3 0.5 0.7 0.9],'xticklabel',{'0.3' '0.5' '0.7' '0.9'})
            xlabel('relative humidity')
        end


    elseif plot_type ==2

        if k == 1           
            bb = axes('position',[L(1) B(imod) W H ]);
            w = 0.5;
        elseif k ==2
            w = 1;
        else
            w = 1.5;
        end

        axes(bb)
        plot(RH_ZBP,T_ZBP,'color',colb,'linewidth',w);
        hold on
        plot(RH,T,'color',col,'linewidth',w)
        hold on
        
        set(gca,'xlim',[0.3 1],'ylim',[180 310],'ydir','reverse')
        set(gca,'ytick',[180 210 240 270 300])

        if imod < length(mods)
            set(gca,'xtick',[0.3 0.5 0.7 0.9]);%,'xticklabel','')
        else
            set(gca,'xtick',[0.3 0.5 0.7 0.9],'xticklabel',{'0.3' '0.5' '0.7' '0.9'})
            xlabel('relative humidity')
        end

       text(0.35,190,model_name,'fontsize',8)
    end

end
end
if plot_type == 1
   print('-dpdf','../Figures/T_profiles.pdf')
elseif plot_type == 2
   print('-dpdf','../Figures/RH_profiles.pdf')
end


%% Plot the simmary figures

fig.bfig(12,20)
set(gcf,'defaultaxesfontsize',10,'defaultaxeslinewidth',0.5,'defaultlinelinewidth',0.75)

subplot(211)
plot(CAPE,CAPE_check,'o')
ylabel('Analytic ZBP (J/kg)')
set(gca,'xlim',[0 5000],'ylim',[0 5000])

subplot(212)
plot(CAPE,CAPE_check2,'o');
hold on
plot([0 6000],[0 6000])
set(gca,'xlim',[0 5000],'ylim',[0 5000])
xlabel('Simulated CAPE (J/kg)')
ylabel('Numerical ZBP CAPE (J/kg)')
a = legend('295','300','305','location','northwest');
set(a,'box','off')

print('-dpdf','../Figures/CAPE_tests.pdf')

fig.bfig(12,12)
plot(epsilon_diag'.*1000,PE_diag','-o')
xlabel('diagnosed PE')
ylabel('diagnosed entrainment (km^{-1})')
print('-dpdf','../Figures/diagnosed.pdf')

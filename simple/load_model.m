function [z,T,p,Tv,qv,RH,rho,model_name] =load_model(varargin)
%
% Load mean variables from RCEMIP simulations
%
%
% [z,T,p,Tv,qv,RH,rho,model_name] =load_model(model_name,SST,sim_type)
%
%
% Inptus:
%
%     model_name = name of model (or number)
%     SST = simulation sea-surface temperature (295, 300, 305)
%     sim_type = 'large' | 'small'
%
%
% Outputs:
%
%    Time- and domain-mean profiles from RCEMIP simulations
%    z, T, p, Tv, qv, RH, rho
%
%    also outputs name of model

%% Input parsing %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Defaults
model_name = 'CM1-LES';
SST = 300;
sim_type = 'small';

% Parse inputs
if nargin >= 1;  model_name = varargin{1}; end
if nargin >= 2;  SST = varargin{2}; end
if nargin >= 3;  sim_type = varargin{3}; end
if nargin >= 4; error('Too many inputs'); end

%% Parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Location of RCEMIP data
data_loc = '../data/';

% Thermodynamics
c = atm.load_constants;

%% Get model data %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% get the model from number if required
if isnumeric(model_name)
    model_list = readlines([data_loc 'models-list_' sim_type '.txt']);
    model_name = model_list{model_name};
    
end

% Set filename
data_loc = [data_loc 'RCE_' sim_type '/']; 
file_name = [data_loc model_name '_RCE_' sim_type num2str(SST) '_cfv0-profiles.nc'];

% Load the file
ncutil.ncload(file_name);

% Get the vars and put them in the right units!
z = zg_avg.*1000;
T = ta_avg;
p = pa_avg.*100;
RH = hur_avg./100;
qv = hus_avg.*0.001;
Tv =  T.*(1 + qv./c.eps - qv);
rho = p./(c.Rd.*Tv);

% Make sure vectors are right way up
% if z(end) < z(1)
% 
%     z = flip(z);
%     T = flip(T);
%     p = flip(p);
%     RH = flip(RH);
%     qv = flip(qv);
%     Tv = flip(Tv);
%     rho = flip(rho);
%     z = flip(z);
% 
% end




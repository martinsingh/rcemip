




output_dir = '/short/wa6/mss565/SAM_output/RCEMIP/';

% RCEMIP_SST295_480x480x146-200m-2s

LES_exp = {'RCEMIP_SST295_480x480x146-200m-2s','RCEMIP_SST300_480x480x146-200m-2s','RCEMIP_SST305_480x480x146-200m-2s'};

CRM_exp = {'RCEMIP_SST295_96x96x74-1000m-8s','RCEMIP_SST300_96x96x74-1000m-8s','RCEMIP_SST305_96x96x74-1000m-8s'};
CRMv_exp = {'RCEMIP_SST295_96x96x146-1000m-8s','RCEMIP_SST300_96x96x146-1000m-8s','RCEMIP_SST305_96x96x146-1000m-8s'};

SST_mat = [295 300 305];

  fig1 = fig.bfig(24,12)
   fig2 = fig.bfig(24,12)
   fig3 = fig.bfig(24,12)
   fig4 = fig.bfig(24,12)

col = 'bgr';

for jj = 1:3


  if jj == 1
     exp = CRM_exp;
  elseif jj == 2
     exp = CRMv_exp;
  else
     exp = LES_exp;
  end
for i = 1:3


   stat_file = [output_dir exp{i} '/OUT_STAT/' exp{i} '.nc'];

   ncutil.ncload(stat_file,'time','RELH','CLD','LWNT','QC','QI','TABS','z')

if i==3
  %return
end

   I = find(time > 25 & time <= 50);



   RH_avg = mean(RELH(:,I),2);
   CLD_avg = mean(CLD(:,I),2);
   QI_avg = mean(QI(:,I),2);
   QC_avg = mean(QC(:,I),2);
   T_avg = mean(TABS(:,I),2);
   
   OLR_avg(i,jj) = mean(LWNT(I));

   figure(fig1)
   subplot(1,3,i)
   plot(CLD_avg,z./1000,col(jj));
   xlabel('cloud fraction')
   set(gca,'xlim',[0 0.3])
  hold on

  set(gca,'ylim',[0 20])
  title(['SST = ' num2str(SST_mat(i)) ' K'])

  box off
   if i ==1
     ylabel('z (km)')
   end

  if jj==3 & i ==1
     l = legend('CRM','vert','LES','location','north')
     set(l,'box','off')
  end


  figure(fig2)
   subplot(1,3,i)
   plot(RH_avg,z./1000,col(jj));
   xlabel('relative humidity')
   set(gca,'xlim',[0 100])
  hold on

  set(gca,'ylim',[0 20])
  title(['SST = ' num2str(SST_mat(i)) ' K'])

  box off
   if i ==1
     ylabel('z (km)')
   end

  if jj==3 & i ==1
     l = legend('CRM','vert','LES','location','north')
     set(l,'box','off')
  end


  figure(fig3)
   subplot(1,3,i)
   if i ==1
     plt(jj) = plot(QC_avg,z./1000, col(jj));
   else
     plot(QC_avg,z./1000, col(jj));
   end
   hold on
   plot(QI_avg,z./1000,['--' col(jj)]);
   xlabel('mean cloud water/ice (g/kg)')
  hold on
  set(gca,'xlim',[0 0.03])
  set(gca,'ylim',[0 20])
  title(['SST = ' num2str(SST_mat(i)) ' K'])

  box off
   if i ==1
     ylabel('z (km)')
   end

  if jj==3 & i ==1
     l = legend(plt,'CRM','vert','LES','location','north')
     set(l,'box','off')
  end

  if jj==1
    Tmean(i,:) = T_avg;
    zmean = z;
  end
  figure(fig4)
   subplot(1,3,i)
   Tmean_int = interp1(zmean,Tmean(i,:),z);
   plot(T_avg-Tmean_int,z./1000, col(jj));
   hold on
   xlabel('Temp anomaly (K)')
  hold on
  set(gca,'ylim',[0 20])
  title(['SST = ' num2str(SST_mat(i)) ' K'])

  box off
   if i ==1
     ylabel('z (km)')
   end

  
  if jj==3 & i ==1
     l = legend(plt,'CRM','vert','LES','location','north')
     set(l,'box','off')
  end



  



end
end

figure(fig1)
print -dpdf ../Figures/cloud_fraction.pdf

figure(fig2)
print -dpdf ../Figures/relative_humidity.pdf


figure(fig3)
print -dpdf ../Figures/cloud_water_and_ice.pdf

figure(fig4)
print -dpdf ../Figures/temperature.pdf

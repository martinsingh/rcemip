% load the data we need

% SST
SST = 295;




% Threshold (g/kg)
q_thresh = 0.01;

% data location
 data_dir = '/g/data/wa6/mss565/SAM_output/RCEMIP_orig/';
%data_dir = '/g/data/k10/mss565/SAM_output/RCEMIP/RCEMIP/';



% THis experiment
 expt = ['RCEMIP_SST' num2str(SST) '_96x96x146-1000m-8s'];
% expt = ['RCEMIP_SST' num2str(SST) '_480x480x146-200m-2s'];

% Get the files
data_dir = [data_dir expt '/OUT_3D/'];
ff = dir([data_dir '*.nc']);
files = {ff.name};

% Read the axes
the_file = [data_dir files{1}];
z = ncread(the_file,'z');

% Initialize the matrices
t = zeros(1,length(files));
cloud_frac = zeros(length(z),length(files));


for i = 1:length(files) 

   % Get the correct file
   disp(num2str(i))
   the_file = [data_dir files{i}];

   % Get the data required
   t = ncread(the_file,'time');
   QN = ncread(the_file,'QN');

   % Fill the vars
   time(i) = t;
   cloud_frac(:,i) = squeeze(mean(mean(QN>q_thresh)));

end

nc = netcdf.create(['SAM_CRM_RCE_small_vert' num2str(SST) '_1D_cldfrac_fixedthreshold_avg.nc'],'clobber');
%nc = netcdf.create(['SAM_CRM_RCE_small_les' num2str(SST) '_1D_cldfrac_fixedthreshold_avg.nc'],'clobber');

  zdim = netcdf.defDim(nc,'z',length(z));
  timedim = netcdf.defDim(nc,'time',length(time));


% Define axes
   timeid = netcdf.defVar(nc,'time','double',timedim);
   netcdf.putAtt(nc,timeid,'units','days');
   netcdf.putAtt(nc,timeid,'long_name','time');

   zid = netcdf.defVar(nc,'z','double',zdim);
   netcdf.putAtt(nc,zid,'units','m');
   netcdf.putAtt(nc,zid,'long_name','height');

   clid = netcdf.defVar(nc,'cldfrac','float',[zdim,timedim]);
    netcdf.putAtt(nc,clid,'long_name','cloud fraction');
    netcdf.putAtt(nc,clid,'definition','points with non-precipitating condensate greater than 0.01 g/kg');
    netcdf.putAtt(nc,clid,'units','fraction');
    netcdf.putAtt(nc,clid,'FillValue_',-9999);

   netcdf.endDef(nc);

   netcdf.putVar(nc,zid,z);
   netcdf.putVar(nc,timeid,time);
   netcdf.putVar(nc,clid,cloud_frac);

    netcdf.close(nc);















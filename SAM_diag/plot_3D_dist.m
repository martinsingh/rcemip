



percentiles = [0.00001 0.0001 0.001 0.01 0.1:0.1:0.9 0.99 0.999 0.9999 0.99999];


output_dir = '/short/wa6/mss565/SAM_output/RCEMIP/';

c = atm.load_constants('SAM');

% RCEMIP_SST295_480x480x146-200m-2s

LES_exp = {'RCEMIP_SST295_480x480x146-200m-2s','RCEMIP_SST300_480x480x146-200m-2s','RCEMIP_SST305_480x480x146-200m-2s'};

CRM_exp = {'RCEMIP_SST295_96x96x74-1000m-8s','RCEMIP_SST300_96x96x74-1000m-8s','RCEMIP_SST305_96x96x74-1000m-8s'};
CRMv_exp = {'RCEMIP_SST295_96x96x146-1000m-8s','RCEMIP_SST300_96x96x146-1000m-8s','RCEMIP_SST305_96x96x146-1000m-8s'};


days = [30 50];
 
col = 'bgr';

for iSIM = 1:3  % type of simulation (LES, CRM, CRMv)


  if iSIM == 1
     exp = CRM_exp;
     dt = 8;
  elseif iSIM == 2
     exp = CRMv_exp;
     dt = 8;
  else
     exp = LES_exp;
     dt = 2;
  end


  for iSST = 1:3	% SST


    threeD_files = dir([output_dir exp{iSST} '/OUT_3D/*.nc']);
    threeD_files = {threeD_files(:).name};


    time = zeros(length(threeD_files),1);
    for i = 1:length(threeD_files)
     
       the_file = [output_dir exp{iSST} '/OUT_3D/' threeD_files{i}];
       time(i) = str2num(the_file(end-12:end-3)).*dt;
       if i ==1
          x = ncread(the_file,'x');
          y = ncread(the_file,'y');
          z = ncread(the_file,'z');
          p = ncread(the_file,'p');

          z_mat = repmat(z,[1 length(x) length(y)]);
          z_mat = permute(z_mat,[2 3 1]);
  
       end
   
    end
     
    I = find(time./86400 > days(1) & time./86400 <= days(2));
    Nt = 0;
    w_mat = zeros(length(z),length(x),length(y),length(I));
    B_mat = zeros(length(z),length(x),length(y),length(I));
    Bn_mat = zeros(length(z),length(x),length(y),length(I));
    h_mat = zeros(length(z),length(x),length(y),length(I));

    T_avg = zeros(length(z),1);
    qv_avg = zeros(length(z),1);
    qn_avg = zeros(length(z),1);
    qp_avg = zeros(length(z),1);
    h_avg = zeros(length(z),1);
    u_avg = zeros(length(z),1);
    v_avg = zeros(length(z),1);


    w_core = zeros(length(z),1);
    h_core = zeros(length(z),1);
    B_core = zeros(length(z),1);
    Bn_core = zeros(length(z),1);
    qn_core = zeros(length(z),1);
    qp_core = zeros(length(z),1);
    fr_core = zeros(length(z),1);

    w_cloud = zeros(length(z),1);
    h_cloud = zeros(length(z),1);
    T_cloud = zeros(length(z),1);
    qv_cloud = zeros(length(z),1);
    B_cloud = zeros(length(z),1);
    Bn_cloud = zeros(length(z),1);
    qn_cloud = zeros(length(z),1);
    qp_cloud = zeros(length(z),1);
    fr_cloud = zeros(length(z),1);

    w_cloub = zeros(length(z),1);
    h_cloub = zeros(length(z),1);
    B_cloub = zeros(length(z),1);
    Bn_cloub = zeros(length(z),1);
    qn_cloub = zeros(length(z),1);
    qp_cloub = zeros(length(z),1);
    fr_cloub = zeros(length(z),1);

    w_clouw = zeros(length(z),1);
    h_clouw = zeros(length(z),1);
    B_clouw = zeros(length(z),1);
    Bn_clouw = zeros(length(z),1);
    qn_clouw = zeros(length(z),1);
    qp_clouw = zeros(length(z),1);
    fr_clouw = zeros(length(z),1);

    w_CDF = zeros(length(z),length(percentiles));
    B_CDF = zeros(length(z),length(percentiles));
    Bn_CDF = zeros(length(z),length(percentiles));
    h_CDF = zeros(length(z),length(percentiles));

 
    for i = 1:length(I)
    
       k = I(i);

       disp(['time: ' num2str(time(k)./86400) ' days'])
       the_file = [output_dir exp{iSST} '/OUT_3D/' threeD_files{k}];
 

       
        w = ncread(the_file,'W');
        u = ncread(the_file,'U');
        v = ncread(the_file,'V');
        T = ncread(the_file,'TABS');
        qv = ncread(the_file,'QV')./1000;
        qn = ncread(the_file,'QN')./1000;
        qp = ncread(the_file,'QP')./1000;

        h = c.cp.*T + c.g.*z_mat + c.Lv0.*qv;



        T_mean = mean(mean(T));
        T_mean = repmat(T_mean,[length(x) length(y) 1]);

        qv_mean = mean(mean(qv));
        qv_mean = repmat(qv_mean,[length(x) length(y) 1]);


        B = (T-T_mean)./T_mean + 0.608.*(qv-qv_mean)  - qn - qp;
        Bn = B+qp; 

        w_mat(:,:,:,i) = permute(w,[3 1 2]);    
        B_mat(:,:,:,i) = permute(B,[3 1 2]);
        Bn_mat(:,:,:,i) = permute(Bn,[3 1 2]);
        h_mat(:,:,:,i) = permute(h,[3 1 2]);

        CORE = w>1 & qn > 1e-5;
        N_CORE = squeeze(sum(sum(CORE)));

        w_core = w_core + squeeze(sum(sum(w.*CORE)))./N_CORE;
        h_core = h_core + squeeze(sum(sum(h.*CORE)))./N_CORE;
        B_core = B_core + squeeze(sum(sum(B.*CORE)))./N_CORE;
        Bn_core = Bn_core + squeeze(sum(sum(Bn.*CORE)))./N_CORE;
        qn_core = qn_core + squeeze(sum(sum(qn.*CORE)))./N_CORE;
        qp_core = qp_core + squeeze(sum(sum(qp.*CORE)))./N_CORE;
        fr_core = fr_core + N_CORE./(length(x).*length(y));

        CLOUD = qn > 1e-5;
        N_CLOUD = squeeze(sum(sum(CLOUD)));

        w_cloud = w_cloud + squeeze(sum(sum(w.*CLOUD)))./N_CLOUD;
        h_cloud = h_cloud + squeeze(sum(sum(h.*CLOUD)))./N_CLOUD;
        T_cloud = T_cloud + squeeze(sum(sum(T.*CLOUD)))./N_CLOUD;
        qv_cloud = qv_cloud + squeeze(sum(sum(qv.*CLOUD)))./N_CLOUD;
        B_cloud = B_cloud + squeeze(sum(sum(B.*CLOUD)))./N_CLOUD;
        Bn_cloud = Bn_cloud + squeeze(sum(sum(Bn.*CLOUD)))./N_CLOUD;
        qn_cloud = qn_cloud + squeeze(sum(sum(qn.*CLOUD)))./N_CLOUD;
        qp_cloud = qp_cloud + squeeze(sum(sum(qp.*CLOUD)))./N_CLOUD;
        fr_cloud = fr_cloud + N_CLOUD./(length(x).*length(y));

        CLOUB = qn > 1e-5 & B > 0;
        N_CLOUB = squeeze(sum(sum(CLOUB)));

        w_cloub = w_cloub + squeeze(sum(sum(w.*CLOUB)))./N_CLOUB;
        h_cloub = h_cloub + squeeze(sum(sum(h.*CLOUB)))./N_CLOUB;
        B_cloub = B_cloub + squeeze(sum(sum(B.*CLOUB)))./N_CLOUB;
        Bn_cloub = Bn_cloub + squeeze(sum(sum(Bn.*CLOUB)))./N_CLOUB;
        qn_cloub = qn_cloub + squeeze(sum(sum(qn.*CLOUB)))./N_CLOUB;
        qp_cloub = qp_cloub + squeeze(sum(sum(qp.*CLOUB)))./N_CLOUB;
        fr_cloub = fr_cloub + N_CLOUB./(length(x).*length(y));

        CLOUW = qn > 1e-5 & w > 0;
        N_CLOUW = squeeze(sum(sum(CLOUW)));

        w_clouw = w_clouw + squeeze(sum(sum(w.*CLOUW)))./N_CLOUW;
        h_clouw = h_clouw + squeeze(sum(sum(h.*CLOUW)))./N_CLOUW;
        B_clouw = B_clouw + squeeze(sum(sum(B.*CLOUW)))./N_CLOUW;
        Bn_clouw = Bn_clouw + squeeze(sum(sum(Bn.*CLOUW)))./N_CLOUW;
        qn_clouw = qn_clouw + squeeze(sum(sum(qn.*CLOUW)))./N_CLOUW;
        qp_clouw = qp_clouw + squeeze(sum(sum(qp.*CLOUW)))./N_CLOUW;
        fr_clouw = fr_clouw + N_CLOUW./(length(x).*length(y));


        Nt = Nt+1;



        T_avg = T_avg + squeeze(T_mean(1,1,:));
        qv_avg = qv_avg + squeeze(qv_mean(1,1,:));
        qn_avg = qn_avg + squeeze(mean(mean(qn)));
        qp_avg = qp_avg + squeeze(mean(mean(qp)));


        u_avg = u_avg + squeeze(mean(mean(u)));
        v_avg = v_avg + squeeze(mean(mean(v)));
        h_avg = h_avg + squeeze(mean(mean(h)));


     end


     % Calculate a few means
     T_avg = T_avg./Nt;
     qv_avg = qv_avg./Nt;
     qn_avg = qn_avg./Nt;
     qp_avg = qp_avg./Nt;
     u_avg = u_avg./Nt;
     v_avg = v_avg./Nt;
     h_avg = h_avg./Nt;

     % CAlculate the conditional statistics
     w_core = w_core./Nt;
     h_core = h_core./Nt;
     B_core = B_core./Nt;
     Bn_core = Bn_core./Nt;
     qn_core = qn_core./Nt;
     qp_core = qp_core./Nt;
     fr_core = fr_core./Nt;

     w_cloud = w_cloud./Nt;
     h_cloud = h_cloud./Nt;
     T_cloud = T_cloud./Nt;
     qv_cloud = qv_cloud./Nt;
     B_cloud = B_cloud./Nt;
     Bn_cloud = Bn_cloud./Nt;
     qn_cloud = qn_cloud./Nt;
     qp_cloud = qp_cloud./Nt;
     fr_cloud = fr_cloud./Nt;

     w_cloub = w_cloub./Nt;
     h_cloub = h_cloub./Nt;
     B_cloub = B_cloub./Nt;
     Bn_cloub = Bn_cloub./Nt;
     qn_cloub = qn_cloub./Nt;
     qp_cloub = qp_cloub./Nt;
     fr_cloub = fr_cloub./Nt;

     w_clouw = w_clouw./Nt;
     h_clouw = h_clouw./Nt;
     B_clouw = B_clouw./Nt;
     Bn_clouw = Bn_clouw./Nt;
     qn_clouw = qn_clouw./Nt;
     qp_clouw = qp_clouw./Nt;
     fr_clouw = fr_clouw./Nt;

     % Calculate the PDFs
     for i = 1:length(z)
        w_CDF(i,:) = quantile(w_mat(i,:),percentiles);
        h_CDF(i,:) = quantile(h_mat(i,:),percentiles);
        B_CDF(i,:) = quantile(B_mat(i,:),percentiles);
        Bn_CDF(i,:) = quantile(Bn_mat(i,:),percentiles);
     end



     date_stamp = date;
     exp_name = exp{iSST};
     save(['../mat_data/w_dist_' exp{iSST} '.mat'],'date_stamp',...
           'w_CDF','B_CDF','Bn_CDF','h_CDF',...
           'w_core','h_core','B_core','Bn_core','qn_core','qp_core','fr_core',...
           'w_cloud','h_cloud','B_cloud','Bn_cloud','qn_cloud','qp_cloud','fr_cloud','T_cloud','qv_cloud',...
           'w_cloub','h_cloub','B_cloub','Bn_cloub','qn_cloub','qp_cloub','fr_cloub',...
           'w_clouw','h_clouw','B_clouw','Bn_clouw','qn_clouw','qp_clouw','fr_clouw',...
           'T_avg','qv_avg','qn_avg','qp_avg','h_avg','u_avg','v_avg',...
           'x','y','z','p','days','Nt','exp_name');

  end % SST




end % SIM






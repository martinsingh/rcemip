




output_dir = '/short/wa6/mss565/SAM_output/RCEMIP/';

% RCEMIP_SST295_480x480x146-200m-2s

LES_exp = {'RCEMIP_SST295_480x480x146-200m-2s','RCEMIP_SST300_480x480x146-200m-2s','RCEMIP_SST305_480x480x146-200m-2s'};

CRM_exp = {'RCEMIP_SST295_96x96x74-1000m-8s','RCEMIP_SST300_96x96x74-1000m-8s','RCEMIP_SST305_96x96x74-1000m-8s'};
CRMv_exp = {'RCEMIP_SST295_96x96x146-1000m-8s','RCEMIP_SST300_96x96x146-1000m-8s','RCEMIP_SST305_96x96x146-1000m-8s'};

SST_mat = [295 300 305];

col = 'bgr';

for jj = 1:3


  if jj == 1
     exp = CRM_exp;
  elseif jj == 2
     exp = CRMv_exp;
  else
     exp = LES_exp;
  end
for i = 1:3

   disp([num2str(jj) ', ' num2str(i)])
   stat_file = [output_dir exp{i} '/OUT_STAT/' exp{i} '.nc'];
   ncutil.ncload(stat_file,'time','PREC','LWNTOA','SWNTOA','LWNTOAC','SWNTOAC','WMAX','z')


   I = find(time > 25 & time <= 50);



   prec_avg(i,jj) = mean(PREC(I));
   
   OLR_avg(i,jj) = mean(LWNTOA(I));
   DSR_avg(i,jj) = mean(SWNTOA(I));
   OLRc_avg(i,jj) = mean(LWNTOAC(I));
   DSRc_avg(i,jj) = mean(SWNTOAC(I));
   wmax_avg(i,jj) = mean(WMAX(I));




  



end
end


fig1 = fig.bfig(10,18)
fig.set_default('paper')
subplot(411)

plot(SST_mat,prec_avg(:,1),'bo-')
hold on
plot(SST_mat,prec_avg(:,2),'go-')
plot(SST_mat,prec_avg(:,3),'ro-')
ylabel('mean precip. (mm/day)')

ll = legend('CRM','vert','LES','location','northwest')
set(ll,'box','off')
box off


subplot(412)
plot(SST_mat,DSR_avg(:,1)-OLR_avg(:,1),'bo-')
hold on
plot(SST_mat,DSR_avg(:,2)-OLR_avg(:,2),'go-')
plot(SST_mat,DSR_avg(:,3)-OLR_avg(:,3),'ro-')
ylabel('Net incoming radiation (W/m^2)')
box off


subplot(413)
plt(1) = plot(SST_mat,DSR_avg(:,1)-OLR_avg(:,1) - (DSRc_avg(:,1)-OLRc_avg(:,1)),'bo-')
hold on
plot(SST_mat,DSR_avg(:,2)-OLR_avg(:,2) - (DSRc_avg(:,2)-OLRc_avg(:,2)),'go-')
plot(SST_mat,DSR_avg(:,3)-OLR_avg(:,3) - (DSRc_avg(:,3)-OLRc_avg(:,3)),'ro-')

plt(2) = plot(SST_mat,-OLR_avg(:,1) +OLRc_avg(:,1),'b-')
plot(SST_mat,-OLR_avg(:,2) +OLRc_avg(:,2),'g-')
plot(SST_mat,-OLR_avg(:,3) +OLRc_avg(:,3),'r-')

plt(3) = plot(SST_mat,DSR_avg(:,1) -DSRc_avg(:,1),'b--')
plot(SST_mat,DSR_avg(:,2) -DSRc_avg(:,2),'g--')
plot(SST_mat,DSR_avg(:,3) -DSRc_avg(:,3),'r--')

ll = legend(plt,'net','LW','SW')
set(ll,'box','off')

ylabel('Cloud-radiative effect (W/m^2)')

subplot(414)
plot(SST_mat,wmax_avg(:,1),'bo-')
hold on
plot(SST_mat,wmax_avg(:,2),'go-')
plot(SST_mat,wmax_avg(:,3),'ro-')
xlabel('SST (K)')
box off

ylabel('max. vert. velocity (m/s)')
print -dpdf ../Figures/fluxes.pdf

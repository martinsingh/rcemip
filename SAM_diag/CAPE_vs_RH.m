

PE = 0:0.01:1;
eps = 0:0.02:4;


[PEm,epsm] = meshgrid(PE,eps);


RH = (1 - PEm + PE.*epsm)./(1+PEm.*epsm);

CAPE = PEm.*epsm./(1+PEm.*epsm);

fig.bfig(15,10)

plot(RH(:,101),CAPE(:,101),'color',[0 0 0.5])
hold on
plot(RH(:,51),CAPE(:,51),'color',[0 0.25 1])
plot(RH(:,21),CAPE(:,21),'color',[0 1 1])

IPE = [1 21 51 101];
Ieps= [1 26 51 101 201];

plot(RH(Ieps(1),IPE),CAPE(Ieps(1),IPE),'o--','color',[0.8 0.8 0.8])
plot(RH(Ieps(2),IPE),CAPE(Ieps(2),IPE),'o--','color',[0.6 0.6 0.6])
plot(RH(Ieps(3),IPE),CAPE(Ieps(3),IPE),'o--','color',[0.4 0.4 0.4])
plot(RH(Ieps(4),IPE),CAPE(Ieps(4),IPE),'o--','color',[0.2 0.2 0.2])
plot(RH(Ieps(5),IPE),CAPE(Ieps(5),IPE),'o--','color',[0 0 0])

box off
xlabel('relative humidity')
ylabel('normalised CAPE')
title('Simple model of Romps (2016)')
l = legend('PE = 1','PE = 0.5','PE = 0.2','\epsilon = 0','\epsilon = 0.5','\epsilon = 1','\epsilon = 2','\epsilon = 4','location','northwest');
set(l,'box','off')

print -dpdf ../CAPE_vs_RH_Romps2016.pdf